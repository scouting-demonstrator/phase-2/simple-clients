#!/bin/bash
simple_dir="/afs/cern.ch/work/g/gpetrucc/vivado/scouting/software/simple-clients"
cmssw_dir="/afs/cern.ch/work/g/gpetrucc/l1p2/l1scout-cmssw/CMSSW_14_0_12/src/L1TriggerScouting/Phase2/test"
vcu128_dir=/afs/cern.ch/work/g/gpetrucc/vivado/vcu118/vcu128
ramdisk=/mnt/ramdisk/$USER/raw
RR=37
n=4
if [[ "$1" != "" ]]; then
    n=$1;
    shift;
fi;
cmsswProcs=6
cmsswProcsHigh=10
cmsswThreads=2
time=10m
receiver=00
receiver_ip=10.0.1.$((100+10*$receiver))
receiver_port=7777
broker_ip=$receiver_ip
broker_port=9876

function launch_cmd {
    name=$1;
    shift;
    host=$1;
    shift;
    if [[ $HOSTNAME == "cms-p2scout-ngt-$host" ]]; then
        screen -t "$name ($host, local)" bash -c "apptainer exec -B /afs -B /eos -B /mnt/ramdisk  docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \$(make env) && $*; echo Done.; read PIPPO'"
    else
        screen -t "$name ($host)" bash -c "ssh cms-p2scout-ngt-$host.cern.ch \"apptainer exec -B /afs -B /eos -B /mnt/ramdisk  docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \\\$(make env) && $*; echo Done.; read PIPPO'\""
    fi;
}

function launch_cmssw {
    name=$1;
    shift;
    host=$1;
    shift; 
    cfg=$1;
    shift; 
    budir=$ramdisk
    cmsswProcsMe=$cmsswProcs
    if [[ $host != $receiver ]]; then
        budir=$(echo $ramdisk | sed s+ramdisk+ngt/ramdisk_$receiver+);
        cmsswProcsMe=$cmsswProcsHigh
    fi
    screen -t "cmsRun $cfg $name ($host)" bash -c "ssh cms-p2scout-ngt-$host.cern.ch 'source /cvmfs/cms.cern.ch/cmsset_default.sh; cd $cmssw_dir && eval \$(scram runtime -sh) &&
         echo cmsRun $cfg buBaseDir=$budir broker=$broker_ip:$broker_port maxEvents=-1 numThreads=$cmsswThreads numFwkStreams=$cmsswThreads buNumStreams=$n $* &&
         python3 cmsMultiRun.py -j $cmsswProcsMe -a 10.0.1.$((100+10*$host)) -p 8082 $cfg buBaseDir=$budir broker=$broker_ip:$broker_port maxEvents=-1 numThreads=$cmsswThreads numFwkStreams=$cmsswThreads buNumStreams=$n $*; 
         echo Done.; read pippo'"
}

function launch_vcu128 {
    ports=$(seq -s ' ' 0 $(((n-1)/2)));
    cports=$(seq -s ',' 0 $(((n-1)/2)));
    screen -t "VCU128: transmit" bash -c "ssh pccmdlab40-22.cern.ch '
        cd $vcu128_dir && source env.sh && 
        uram_tcp_tool.py -c \$c do x0 tcp_eth_reset $ports && 
        for I in $ports; do 
            uram_tcp_tool.py -c \$c do x0 tcp\${I}_config ip_src 10.0.1.\$((10 * \$I + 1)) ip_dst $receiver_ip  port0_dst \$(( $receiver_port + 2*\$I )) port1_dst \$(( $receiver_port + 1 + 2*\$I )) && 
            uram_tcp_tool.py -c \$c do x0 tcp\${I}_stream_ctrl enable 0 \$(($n-2*\$I>1));
        done && 
        python3.8 prometheus_exporter.py -c \$c uramtcp:$cports -b && 
        uram_tcp_tool.py -c \$c do x0 tcp_status $ports && 
        uram_tcp_tool.py -c \$c do x0 tcp_connect $ports && 
        uram_tcp_tool.py -c \$c do x0 tcp_ctrl transmit $ports &&
        uram_tcp_tool.py -c \$c do x0 tcp_watch $ports timeout=$time; 
        uram_tcp_tool.py -c \$c do x0 tcp_ctrl stop $ports
    '; echo Done; read pippo"
}

### Cleanup
for host in 00 01 02 03 04; do ssh cms-p2scout-ngt-$host.cern.ch "
    pkill -u$USER cmsRun 2> /dev/null || true; 
    rm $ramdisk/run0000$RR/*raw 2> /dev/null || true;
    " > /dev/null 2>&1; 
done

## Spaw receiver
launch_cmd "Receiver" $receiver ./p2scout-client  DTHRollingReceive256MM $receiver_ip:$receiver_port $ramdisk --orbitBitsPerFile 12 --run $RR  --prometheus $receiver_ip:8080 -n $n --cmssw -O 256

## Spawn file broker (note: we need strict checking here)
#launch_cmd "Broker" $receiver ./p2scout-fileBroker -a $broker_ip -p $broker_port $ramdisk/run0000$RR  --prometheus $broker_ip:8084  --stream0
launch_cmd "Broker" $receiver ./p2scout-fileBroker -a $broker_ip -p $broker_port $ramdisk/run0000$RR  --prometheus $broker_ip:8084  --demux $n --checkEvents
sleep 2

## Spawn CMSSW
launch_cmssw "Proc0" 00 runScoutingPhase2Puppi_cfg.py run=selected tmuxPeriod=6 timeslices=$n
launch_cmssw "Proc1" 01 runScoutingPhase2Puppi_cfg.py run=selected tmuxPeriod=6 timeslices=$n
launch_cmssw "Proc2" 02 runScoutingPhase2Puppi_cfg.py run=selected tmuxPeriod=6 timeslices=$n
launch_cmssw "Proc3" 03 runScoutingPhase2Puppi_cfg.py run=selected tmuxPeriod=6 timeslices=$n
launch_cmssw "Proc4" 04 runScoutingPhase2Puppi_cfg.py run=selected tmuxPeriod=6 timeslices=$n

## Spawn data generators
sleep 5
launch_vcu128