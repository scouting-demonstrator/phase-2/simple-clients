#!/bin/bash
simple_dir="/afs/cern.ch/work/g/gpetrucc/vivado/scouting/software/simple-clients"
vcu128_dir=/afs/cern.ch/work/g/gpetrucc/vivado/vcu118/vcu128
ramdisk=/mnt/ramdisk/$USER/raw
RR=37
n=6
time=20m
receiver_port=7770
#LINKS=eth25rx:0-23   # define this to have it scraped to prometheus too
LINKS=csprx:0-23   # define this to have it scraped to prometheus too

function launch_cmd {
    name=$1;
    shift;
    host=$1;
    shift;
    if [[ $HOSTNAME == "cms-p2scout-ngt-$host" ]]; then
        screen -t "$name ($host, local)" bash -c "apptainer exec -B /afs -B /eos -B /mnt/ramdisk  docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \$(make env) && $*; echo Done.; read PIPPO'"
    else
        screen -t "$name ($host)" bash -c "ssh cms-p2scout-ngt-$host.cern.ch \"apptainer exec -B /afs -B /eos -B /mnt/ramdisk  docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \\\$(make env) && $*; echo Done.; read PIPPO'\""
    fi;
}

function launch_vcu128 {
    dests="$*"
    ports="0 1 2";
    cports="0,1,2";
    screen -t "VCU128: transmit" bash -c "ssh pccmdlab40-22.cern.ch '
        cd $vcu128_dir && source env.sh && 
        uram_tcp_tool.py -c \$c do x0 tcp_eth_reset $ports && 
        I=0 &&
        for H in $dests; do 
            receiver_ip=10.0.1.\$((100+10*\$H));
            uram_tcp_tool.py -c \$c do x0 tcp\${I}_config ip_src 10.0.1.\$((10 * \$I + 1)) ip_dst \$receiver_ip port0_dst \$(( $receiver_port + 2*\$I )) port1_dst \$(( $receiver_port + 1 + 2*\$I )) && 
            uram_tcp_tool.py -c \$c do x0 tcp\${I}_stream_ctrl enable 0 1;
            I=\$((I+1));
        done && 
        python3.8 prometheus_exporter.py -b -c \$c uramtcp:$cports $LINKS && 
        uram_tcp_tool.py -c \$c do x0 tcp_status $ports && 
        uram_tcp_tool.py -c \$c do x0 tcp_connect $ports && 
        uram_tcp_tool.py -c \$c do x0 tcp_ctrl transmit $ports &&
        uram_tcp_tool.py -c \$c do x0 tcp_watch $ports timeout=$time; 
        uram_tcp_tool.py -c \$c do x0 tcp_ctrl stop $ports
    '; echo Done; read pippo"
}

### Cleanup
for host in 00 01 02 03 04; do ssh cms-p2scout-ngt-$host.cern.ch "
    rm $ramdisk/run0000$RR/*raw 2> /dev/null || true;
    ps x | awk '/p2scout-(fileW|client)/{print \$1}' | xargs kill;
    " > /dev/null 2>&1; 
done

## Spaw receivers
function launch_receiver {
    host=$1;
    shift;
    stream=$1;
    shift;
    ip=10.0.1.$((100 + 10*$host))
    port=$(($receiver_port + 2*$stream))
    launch_cmd Receiver $host ./p2scout-client DTHRollingReceive256MM $ip:$port $ramdisk --orbitBitsPerFile 10 --run $RR --prometheus $ip:8080 -n 2 --cmssw -O 256 -t stream
}
launch_receiver 00 0
launch_receiver 01 1
launch_receiver 02 2

## Spawn file deleters (note: we need strict checking here)
launch_cmd "Deleter" 00 ./p2scout-fileWaiter $ramdisk/run0000$RR delete
launch_cmd "Deleter" 01 ./p2scout-fileWaiter $ramdisk/run0000$RR delete
launch_cmd "Deleter" 02 ./p2scout-fileWaiter $ramdisk/run0000$RR delete
#sleep 2

## Spawn data generators
sleep 5
launch_vcu128 00 01 02
#launch_cmd "Generator" 00 ./p2scout-generator DTHBasic256 root/data/Puppi.dump 10.0.1.100:7770 -n 2 --sync --time $time
#launch_cmd "Generator" 01 ./p2scout-generator DTHBasic256 root/data/Puppi.dump 10.0.1.110:7772 -n 2 -t 2 --sync --time $time
#launch_cmd "Generator" 03 ./p2scout-generator DTHBasic256 root/data/Puppi.dump 10.0.1.130:7774 -n 2 -t 4 --sync --time $time