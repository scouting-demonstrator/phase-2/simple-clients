#!/bin/bash
simple_dir="/afs/cern.ch/work/g/gpetrucc/vivado/scouting/software/simple-clients"
cmssw_dir="/afs/cern.ch/work/g/gpetrucc/l1p2/l1scout-cmssw/CMSSW_14_0_9/src/L1TriggerScouting/Phase2/test"
ramdisk=/mnt/ramdisk/$USER/raw
outdisk=/mnt/ramdisk/$USER/root
anadisk=/mnt/ramdisk/$USER/analysis
RR=37
n=4
time=2m
receiver=00
receiver_ip=10.0.1.$((100+10*$receiver))
broker_ip=$receiver_ip
broker_port=9876
unpacker_obj=puppi
unpacker_type=ttree
unpacker_format=float

analysis_hosts="00 01 03 04"
unpacker_ncpus=16
analsys_ncpus=10

function launch_cmd {
    name=$1;
    shift;
    host=$1;
    shift;
    if [[ $HOSTNAME == "cms-p2scout-ngt-$host" ]]; then
        screen -t "$name ($host, local)" bash -c "apptainer exec -B /afs -B /eos -B /mnt/ramdisk -B /mnt/ngt docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \$(make env) && $*; echo Done.; read PIPPO'"
    else
        screen -t "$name ($host)" bash -c "ssh cms-p2scout-ngt-$host.cern.ch \"apptainer exec -B /afs -B /eos -B /mnt/ramdisk -B /mnt/ngt docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0.1 bash -c 'echo $*; cd $simple_dir && eval \\\$(make env) && $*; echo Done.; read PIPPO'\""
    fi;
}

function launch_unpacker  {
    name=$1;
    shift;
    host=$1;
    shift;
    budir=$ramdisk
    if [[ $host != $receiver ]]; then
        budir=$(echo $ramdisk | sed s+ramdisk+ngt/ramdisk_$receiver+);
    fi
    launch_cmd $name $host  ./p2scout-liveUnpacker --cmssw $unpacker_obj $unpacker_type $unpacker_format $broker_ip:$broker_port $budir/run0000$RR $outdisk/run0000$RR --prometheus 10.0.1.$((100+10*$host)):8081  $*
}

function launch_analysis  {
    name=$1;
    shift;
    host=$1;
    shift;
    budir=$ramdisk
    if [[ $host != $receiver ]]; then
        budir=$(echo $ramdisk | sed s+ramdisk+ngt/ramdisk_$receiver+);
    fi
    launch_cmd $name $host  ./root/rdf_analysis/p2scout-liveAnalysis $* $outdisk/run0000$RR $anadisk/run0000$RR --prometheus 10.0.1.$((100+10*$host)):8082 
}


### Cleanup
for host in 00 01 02 03 04; do ssh cms-p2scout-ngt-$host.cern.ch "
    rm $ramdisk/run0000$RR/* $outdisk/run0000$RR/* $anadisk/run0000$RR/* 2> /dev/null || true;
    pkill -9 -f -u$USER p2scout-live 2> /dev/null || true;
    " > /dev/null 2>&1; 
done

## Spaw receiver
launch_cmd "Receiver" $receiver ./p2scout-client  DTHRollingReceive256MM $receiver_ip:7777 $ramdisk --orbitBitsPerFile 10 --run $RR  --prometheus $receiver_ip:8080 -n $n --cmssw -O 256

## Spawn file broker
launch_cmd "Broker" $receiver ./p2scout-fileBroker -a $broker_ip -p $broker_port $ramdisk/run0000$RR  --prometheus $broker_ip:8084  --stream0

sleep 2

## Spawn unpackers
for H in $analysis_hosts; do
    launch_unpacker "Unpacker" $H --demux $n -j $unpacker_ncpus 
done

sleep 1

## Spawn analysis (on same hosts)
for H in $analysis_hosts; do
    launch_analysis "Analysis" $H w3piExample2022 loose -f tree -o snapshot -j $analsys_ncpus 
done

sleep 2

## Spawn data generators
sleep 5
launch_cmd "Generator" 03 ./p2scout-generator DTHBasic256 root/data/Puppi.dump $receiver_ip:7777  -n $n --sync --time $time