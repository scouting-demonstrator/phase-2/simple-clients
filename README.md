# Client code for Phase-2 L1 Data Scouting

This repository contains software clients to receive and process Phase-2 L1 Data Scouting data.
 * Main directory: C++ clients to receive scouting data via TCP/IP
 * `root` directory: clients to unpack data to [ROOT](https://root.cern.ch) [TTree](https://root.cern.ch/doc/master/classTTree.html) or [RNTuple](https://root.cern/doc/master/classROOT_1_1Experimental_1_1RNTuple.html) formats.
 * `apache` directory: clients to unpack data to [Apache Arrow](https://arrow.apache.org/docs/cpp/index.html) format

 ## Raw event formats

 * **Native64**: each event has a 64 bit event header followed by N Puppi candidates (64 bits each). The event header format is

   | bits  | size | meaning |
   |-------|------|---------|
   | 63-62 |   2  |  `10` = valid event header |
   | 61    |   1  | error bit |
   | 60-56 |   5  | local run number (to be defined later) |
   | 55-24 |  32  | orbit number (normally >= 1) |
   | 23-12 |  12  | bunch crossing number - 1 (i.e. 0-3563) |
   | 11-00 |  12  | number of 64 bit words (=number or Puppi candidates) |

      * Note that for Puppi candidates, the max number is 208 so only 8 bits of the size are ever used.
      * An event is identified  _truncated_ if it's header reports a length of 0 candidates, and the error bit is set.

* **DTHBasic256**: each orbit is split in one or more DTH v1p2-256 data blocks. All blocks except the last have fixed size of 8 kB. Each block starts with a [256 bit DTH v1p2 header](https://gitlab.cern.ch/scouting-demonstrator/phase-2/p2-scouting-firmware/-/blob/master/components/dth_v1p2-tcp/firmware/hdl/daq-tcp/frag_to_blocks/Memory_blocks_256_noSR.vhd?ref_type=heads) (the format is the same as the 128 bit DTH v1p2 header below plus another 128 bits of zeros, but the size is in units of 256 bits instead of 128 bits). The DTH is followed by events in the **Native64** format as above, and there may be a padding of zeros to make the orbit be a multiple of 256 bits. Note that events can be broken across two DTH data blocks. 

  * A small test file in this format is available under `data/tcp_gen4_dth256.data`
  * A _truncated_ orbit has only one Orbit header with the error bit set to 1 and size set to zero, i.e. 
  
   | bits  | size | meaning |
   |-------|------|---------|
   | 63-62 |   2  |  `11` = valid orbit header |
   | 61    |   1  | error bit |
   | 60-56 |   5  | (local) run number |
   | 55-24 |  32  | orbit number |
   | 23-00 |  24  | size in 64 bit words |

 * **CMSSW**: Events are in *Native64* format as before, but there are additional per-file and per-orbit headers.
  * A _truncated_ orbit has only the per-orbit header, with a payload size of zero.

The *per-file header* is 32 bytes long, and should match the definitions in [FRDFileHeaderContent_v2](https://github.com/cms-sw/cmssw/blob/master/IOPool/Streamer/interface/FRDFileHeader.h)

   | bytes  | size | meaning |
   |-------|------|---------|
   | 0-7 |   8  |   `"RAW_0002"` in ASCII (`0x52, 0x41, 0x57, 0x5f 0x30, 0x30, 0x30, 0x32`) | 
   | 8-9 | 2 | Header size in bytes (should be `32`) |
   | 10-11 | 2 | Event type (should be `20` ) |
   | 12-15 | 4 | Number of "events" (orbits in our case) |
   | 16-19 | 4 | Run number |
   | 20-23 | 4 | Lumisection number |
   | 24-31 | 8 | File size in bytes (including all headers) |

The *per-orbit header*  is 24 bytes long, and should match the definitions in [FRDEventHeader_V6](https://github.com/cms-sw/cmssw/blob/master/IOPool/Streamer/interface/FRDEventMessage.h)

   | bytes  | word32 | size | meaning |
   |-------|--|----|---------|
   | 0-1 | 0h |  2  |   Version (`06`) | 
   | 2-3 | 0l | 2 | Flags (`00`) |
   | 4-7 | 1 | 4 | Run number |
   | 8-11 | 2 | 4 | Lumisection number |
   | 12-15 | 3| 4 | "Event" number (orbit number in our case) |
   | 16-19 | 4 | 4 | Payload size in bytes (__NOT__ including headers) |
   | 20-23 |5 | 4 | CRC32C (not used) |
 
 ### Obsolete formats

 * **Native128**: each event has a 64 bit event header followed by N Puppi candidates (64 bits each), and is padded to a multiple of 128 bits. 
    * An event is identified  _truncated_ if it's header reports a length of 0 candidates, but the following padding word is not zero.
 * **DTHBasic**: each event has a 128 bit [DTH v1p2 header](https://gitlab.cern.ch/dth_p1-v2/dth_2srs_2srr_1daq/-/blob/master/top.srcs/sources_1/new/frag_to_blocks/Memory_blocks.vhd), a 128 bit [SlinkRocket](https://edms.cern.ch/file/2502737/2/cms_phase2_slinkrocket.pdf) header, the event padded to 128 bits (as **Native128** above), and a SlinkRocket trailer. 
 * **DTHBasicOA**: each orbit is split in one or more DTH v1p2 data blocks. All blocks except the last have fixed size of 4 kB. Each block starts with a [128 bit DTH v1p2 header](https://gitlab.cern.ch/dth_p1-v2/dth_2srs_2srr_1daq/-/blob/master/top.srcs/sources_1/new/frag_to_blocks/Memory_blocks.vhd). The orbit data has a 128 bit [SlinkRocket](https://edms.cern.ch/file/2502737/2/cms_phase2_slinkrocket.pdf) header, the events in **Native128** format, and a SlinkRocket trailer. Note that events can be broken across two DTH data blocks. 
   * A _truncated_ orbit has only the SlinkRocket header and trailer, and no events inside.
 * **DTHBasicOA-NoSR**: same as above, but with no SlinkRocket headers

Two small example data files in **DTHBasic** and **DTHBasicOA** formats are available in the `data` directory.

## Compiling the code

The standalone code in this directory depends only on a recent gcc. However, the subdirectories `root` and `apache` depend on having root and apache arrow available. 
By default, the compilation will build also the root and apache subsystems, you can switch this off setting `USE_APACHE=0` and `USE_ROOT=0` in your environment.

### Compiling in a container

One possible approach to get an environment to compile the code is to use the docker images compiled for this repository, from the [gitlab container registry](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/container_registry). This is also the preferred solution if you just need to run the code, and not compile it.

If your source code is on /afs (or on /eos), you can just open an apptainer shell inside the container, binding the /afs and /eos mounts from outside with
```bash
export APPTAINER_CACHEDIR=/tmp/$USER/cache ## Configure temporary directory
apptainer shell -B /afs -B /eos docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0
cd /afs/cern.ch/path/to/the/code/...
```
if your source is in some local directory, you can bind that one as well, e.g. bind the current directoory to the path `/src` within the container.
```bash
apptainer shell -B $PWD:/src -B /afs -B /eos docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9:1.0
cd /src
```

### Compiling using LCG releases from CVMFS

An easy way to set up your environment on an **EL9** machine with CVMFS like lxplus9.cern.ch is to source the LCG development environment [LCG_106](https://lcginfo.cern.ch/release_packages/106/x86_64-el9-gcc13-opt/), which you can do with 
```bash
eval $(make envlcg106)
```
you can also try the [dev3 nightly build](https://lcginfo.cern.ch/release_packages/dev3/x86_64-el9-gcc13-opt/) with the master branch of ROOT with `eval $(make envlcgdev)`, but note that you'll have to recompile everything each day.


If you're on an EL8 machine, you can run an EL9 container with apptainer using
```bash
apptainer shell -B /eos -B /afs -B /cvmfs /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/sft/docker/alma9:latest
```
and then inside set up the LCG environment from /cvmfs

## Main directory

This directory contains a C++ client `p2scout-client` that can receive data via TCP/IP or read it from files, and check different formats, and a dummy data generator `p2scout-generator` that can output data.

```bash
make -j 8 && make run_tests
```

### Data checker & receiver

```bash
./p2scout-client [options] Mode Source [arguments]
```

*Source* can be a file, a TCP/IP "address:port", or a named fifo. If a `%d` is found in the file name, it's replaced by the index of the client (starting from zero).

*Modes* that receive the data and perform some validity checks on it are:
 * `Native64`: files in the formats described above
 * `Native64SZ`: file in `Native64` format but allowing possible null 64 bit words between events
 * `DTHBasic256`, `DTHBasic256-NC`: read DTHBasic256 format. The `-NC` version only checks DTH headers and not the contents of the orbit payload itself.
 * Some obsolete modes also exist: `Native128`, `DTHBasic`, `DTHBasicOA`, `DTHBasicOA-NC`, `DTHBasicOA-NoSR`


*Modes* that receive and store some data are:
 * `DTHRollingReceive256` and `DTHRollingReceive256MM`: when used as `p2scout-client DTHRollingReceive256[MM] source outputBasePath  [--prescale N]`  it will receive in `DTHBasic256` format and saves data out in `Native64` format with up to 3 null 64-bit words at the end of each orbit (so, it can be read back with `Native64SZ`). 
  * CMSSW file and event headers can be added by passing the options `--cmssw --run NUMBER`
  * Each file will contain by default 2^12 orbits (the exponent 12 can be configured with `--orbitBitsPerFile N`, default 12). 
  * Outputs are saved as _outputBasePath_ + `/runNNNNNN/runNNNN_lsNNNN_indexNNNNNN_streamNN.dump` where _run_ is the run number (set with `--runNumber`, default 0), _ls_ is the lumisection number (`1 + (orbit-1)/2^18`, the 18 can be changed with `--orbitBitsPerLS`), _index_ contains the lower bits of the orbit number of the first orbit in the file, and `stream` is the stream number (option `-i`, defaults to the timeslice index, argument of option `-t`). The file is first created with extension `.raw.tmp` and then renamed to `.raw` after it is closed.
  * The `DTHRollingReceive256MM` memory-maps the output file, so it's a bit faster, but still being tested
 * Some obsolete modes also exist:
    * `DTHReceive256`: when used as `p2scout-client DTHReceive256 source output [--prescale N]` it will receive in `DTHBasic256` format and saves a prescaled amount of orbits out in `Native64` format with up to 3 null 64-bit words at the end of each orbit (so, it can be read back with `Native64SZ`). It stops after collecting 4GB of data.
    * `DTHReceiveOA`: when used as `p2scout-client DTHReceiveOA source output [--prescale N]` it will receive in `DTHBasicOA` format and saves a prescaled amount of orbits out in `Native128` format. It stops after collecting 4GB of data.

*Modes* for debugging are
 * `TrashData`: receive data via TCP/IP and discard it without any processing
 * `ReceiveAndStore`: when used as `p2scout-client ReceiveAndStore source sizeInGB outputFile`: receive data up to _sizeInGB_ data and save it as in _outputFile_ without any processing.

Useful *options* are:
 * `-h`: lists all the options
 * `-t`: specifies the timeslice for the stream (default: 0)
 * `-T`: specifies the tmux period (default: 6)
 * `-n`: receives `n` streams instead of 1. For TCP/IP, the streams receive on consecutive ports, so receiving e.g. on port 7777 with `-n 2` will open a server on 7777 and one on 7778. For files, a `%d` has to be specified in the file name. 
  * Normally, the timeslice index is incremented for each stream, so stream 0 expects timeslice 0, stream 1 expects timeslice 1, etc (this is not done if `--orbitmux N` is specified with N > 1)
 * `--prometheus <addr>:<port>` expose Prometheus-compatible metrics on received data etc.
 * `-k`: restart the servers after the connection is closed (use `<ctrl>+c` to terminate the job)
 * `--maxorbits N`: stops after processing N orbits


### Event generator

The even generator reads some events in `Native64` format, and resamples them in order to generate a stream of data. Data is normally resampled pseudo-randomly, but it is also possible to just replay thee data in the same order it was (looping back to the beginning when the end is reached) by setting the random number generator seed to zero (`--seed 0`)

```bash
./p2scout-generator [options] Mode root/data/Puppi.dump Destination
```

Supported *modes* at the moment are just `Native64`, `CMSSW` and `DTHBasic256`

*Destinations* can be files or fifos (possibly with a `%d` in the name, when generating multiple streams), or `ip:port` for TCP connections.

Useful *options* are:
 * `-T`: specifies the tmux period (default: 6)
 * `-n N`: generate N streams of data
 * `--orbits N`: specifies how many orbits to generate
 * `--time T[s|m|h]`: generates orbits corresponding to T seconds, minutes or hours (can be used instead of --orbits)
 * `--sync`: try to generate the orbits at the LHC rate (3564 / 40MHz); actual rate may be slower if the system is not fast enough to sustain the LHC rate

### Example running generator and receiver using docker image via apptainer (singularity)

Docker images for this package are available in the gitlab container registry: https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/container_registry.
The best way to run them interactively is to use apptainer (formerly known as singularity), 
as that uses directly the network of the host without intermediate layers.

```bash
## Configure temporary directory
export APPTAINER_CACHEDIR=/tmp/$USER/cache
## Start a data checker receiver, with 6 streams, on ip 10.0.1.100
apptainer exec docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9-full:1.0 p2scout-client DTHBasic256 10.0.1.100:7777 -n 6
## Start a data generator to send data to that ip using 6 streams, for 10 seconds, at LHC data rate.
apptainer exec docker://gitlab-registry.cern.ch/scouting-demonstrator/phase-2/simple-clients/el9-full:1.0 p2scout-generator DTHBasic256 /data/Puppi.dump 10.0.1.100:7777 --time 10s --sync -n 6
```

Podman also works, using -p to bind network ports of the host to those inside of the container, but at least when runnig it in non-privileged mode there seems to be a substantial overhead in the network I/O.

## Data unpackers

Unpackers read data from `Native64` format or `CMSSW` format and convert it to ROOT, Arrow etc.

The base class for all unpackers is [UnpackerBase](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/-/blob/master/UnpackerBase.h) provided in the main directory, but the specific implementations for ROOT and Apache Arrow are under the respective subdirectories [root](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/-/tree/master/root) and [apache](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/-/tree/master/apache).

### Single-file unpacker (p2scout-unpacker)
The main binary to run the unpackers is `p2scout-unpacker`, that can be run as 
```bash
./p2scout-unpacker [options] <objType> <fileType> <format> input1 [input2 input3 ...] [output]
```
 * _objType_ is `puppi` or `tkmu`
 * _fileType_ is `ttree` or `rntuple` for the root unpackers, and `ipcstream` or `ipcfile` for the apace ones
 * _format_ is a specific choice given the file type and object type choosen
 * _common options_ are:
   * `--cmssw`: input file has CMSSW headers
   * `-z algo[,level]` (or `--compression`): applies a specific compression (e.g. `lz4,4`)
   * `-j N` (or `--threads N`): uses N threads, though typically only for the compression part.
 * the command line recognises whether a file on the command line is an input or output dependig on the extension (`.root` and `.arrow` are outputs) 

More documentation, including the different formats supppored for each object kind are in the pages for the concrete implementations: [root](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/-/blob/master/root/README.md), [apache](https://gitlab.cern.ch/scouting-demonstrator/phase-2/simple-clients/-/blob/master/apache/README.md)


### Realtime unpacker (p2scout-liveUnpacker)

This tool monitors an input directory for new files, unpacks them to the output directory and deletes the input. It can then be run as 
```bash
./p2scout-liveUnpacker [options] <objType> <fileType> <format> /path/to/input /path/to/outputs 
```
 * _objType_, _fileType_, _format_ and most options are the same as for the simple unpacker documented above.
 * Some options behave differently or are new
    * `-j N`: run multithread using up to N cpus to unpack files in parallel as they arrive.
      * FIXME: that the multithreaded live unpacker doesn't seem to respond to _ctrl+C_, so the way to stop it is to send it to background with _ctrl+Z_ and then `kill -9 %`. 
    * `--delete`: delete also the _output_ files once the unpacking is completed (this is just for benchmarking, so that one doesn't fill up the ramdisk if there's no consumer process)
    * `--demux T`: time-demultiplex inputs that arrive from T timeslices. Input filenames must conform to what DTHRollingReceive256 produces.
    * `--prometheus <addr>:<port>` expose Prometheus-compatible metrics.


To avoid race conditions, the unpacker starts when a new file is either *moved to* the input directory, or *closed after being open for writing*, and will only process files that end with `.raw`.
The file is renamed to `.raw.taken.<hostname>` before starting the processing, and the output is called `.tmp.<extension>` until it's complete.

### FileBroker for CMSSW live processing

This simple HTTP file broker serves files for CMSSW to run live processing on using the DAQSource in DataModeScoutingPhase2 mode.

```bash
./p2scout-fileBroker [options] /path/to/raw/files [ --stream0 | --demux N]
```

*Options* that receive the data and perform some validity checks on it are:
 * `-a <address>`: address to listen to
 * `-p <port>`: tcp port to listen to
 * `--stream0`: only check and serve files for stream 0; useful if any of the other streams go on different ram disks or nodes, as the CMSSW source will anyway wait for the additional files for the other streams startng the processing
 * `--demux <N>`: check that files for all _N_ streams are found for a given set of orbits before serving the stream0 file: this may be of use if sometimes a stream doesn't produce a file, as otherwise CMSSW will hang and then crash when not finding the file.
 * `--prometheus <addr>:<port>` expose Prometheus-compatible metrics.

### Directory watcher & cleaner

```bash
./p2scout-fileWaiter <director> [delete]
```

This simple demonstration, built from `utils/file_waiter.cc`,  shows how to monitor a directory for new files. If passed the string `delete` as second argument, it will delete new files that are moved into that directory, and so it can be used to test the rolling receiver or live unpacker without filling up disks.