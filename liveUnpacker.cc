#include <atomic>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <deque>
#include <filesystem>
#include <iostream>
#include <list>
#include <thread>
#include <string_view>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <libgen.h>
#include <tbb/pipeline.h>
#include <getopt.h>
#include "UnpackMaker.h"
#include "unpack.h"
#include "prometheusUtils.h"
#ifdef USE_ROOT
  #include <TROOT.h>
#endif

void usage() {
  printf(
      "Usage: p2scout-liveUnpacker [ options ] <obj> <kind> <format> [broker_addr:port] /path/to/input "
      "/path/to/outputs \n");
  printf("See p2scout-unpacker --help for obj, kind, format and most of the options\n");
  printf("Options: \n");
  printf("  --demux N       : demux N streams\n");
  printf("  -j N            : multithread with N threads\n");
  printf("  --delete        : delete unpacked output (for benchmarking without filling disks)\n");
  printf("  --orbitBitsPerLS N : log2(Orbits) per lumisection (default 18, i.e. 256k orbits per 23s lumisection)\n");
  printf("  --prometheus addr : Export stats as prometheus endpoint on addr (e.g. 127.0.0.1:8081)\n");
}

struct Token {
  Token() : inputs(), output() {}
  Token(const std::string &inputName, const std::string &outputName) : inputs(1, inputName), output(outputName) {}
  Token(const std::vector<std::string> &inputNames, const std::string &outputName)
      : inputs(inputNames), output(outputName) {}
  std::vector<std::string> inputs;
  std::string output;
};

struct Totals {
  std::chrono::time_point<std::chrono::steady_clock> tstart;
  std::atomic_bool started;
  std::atomic<unsigned> jobs;
  std::atomic<unsigned long long> events, kb_in, kb_out;
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
  prometheus::Counter &cntEvents, &cntBytesIn, &cntBytesOut, &cntFiles;

  Totals(prometheus::Registry &registry)
      : started(false),
        jobs(0),
        events(0),
        kb_in(0),
        kb_out(0),
        cntEvents(prometheus::BuildCounter().Name("unpacker_events_total").Register(registry).Add({})),
        cntBytesIn(prometheus::BuildCounter().Name("unpacker_read_bytes_total").Register(registry).Add({})),
        cntBytesOut(prometheus::BuildCounter().Name("unpacker_wrote_bytes_total").Register(registry).Add({})),
        cntFiles(prometheus::BuildCounter().Name("unpacker_wrote_files_total").Register(registry).Add({})) {}
#else
  Totals() : jobs(0), events(0), kb_in(0), kb_out(0) {}
#endif

  void maybe_start() {
    if (jobs.load() == 0) {
      tstart = std::chrono::steady_clock::now();
      started = true;
    }
  }

  void add(const UnpackerBase::Report &r, bool print = true) {
    jobs.fetch_add(1);
    events.fetch_add(r.entries);
    kb_in.fetch_add(std::round(r.bytes_in / 1024));
    kb_out.fetch_add(std::round(r.bytes_out / 1024));
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
    cntEvents.Increment(r.entries);
    cntBytesIn.Increment(r.bytes_in);
    cntBytesOut.Increment(r.bytes_out);
    cntFiles.Increment(1);
#endif
    if (print) {
      auto tend = std::chrono::steady_clock::now();
      auto dt = (std::chrono::duration<double>(tend - tstart)).count();
      double GB_in = kb_in.load() / 1024.0 / 1024.0;
      double GB_out = kb_out.load() / 1024.0 / 1024.0;
      printf("Tot of %.1fs, %u jobs, %llu events, %.3f GB in (%.1f GB/s), %.3f GB out (%.1f GB/s)\n",
             dt,
             jobs.load(),
             events.load(),
             GB_in,
             GB_in / dt,
             GB_out,
             GB_out / dt);
    }
  }
};

class Executor {
public:
  Executor(const UnpackMaker::Spec &unpackerSpec, Totals &totals, bool deleteAfterwards)
      : spec_(unpackerSpec), totals_(&totals), deleteAfterwards_(deleteAfterwards) {}
  Executor(const Executor &other)
      : spec_(other.spec_), totals_(other.totals_), deleteAfterwards_(other.deleteAfterwards_) {}
  void operator()(Token token) const {
    if (token.inputs.empty())
      return;
    std::unique_ptr<UnpackerBase> unpacker = UnpackMaker::make(spec_);
    assert(unpacker);
    std::string extension = unpacker->outputExtension();
    std::string tmpOut = token.output + ".tmp." + extension, finalOut = token.output + "." + extension;
    printf("Unpack %s[#%d] -> %s\n", token.inputs.front().c_str(), int(token.inputs.size()), tmpOut.c_str());
    auto report = unpacker->unpackFiles(token.inputs, tmpOut);
    printReport(report);
    totals_->add(report);
    for (const auto &in : token.inputs)
      unlink(in.c_str());
    if (deleteAfterwards_) {
      printf("Deleting %s\n", tmpOut.c_str());
      unlink(tmpOut.c_str());
    } else {
      rename(tmpOut.c_str(), finalOut.c_str());
      printf("Renaming output to %s\n", finalOut.c_str());
    }
  }

private:
  UnpackMaker::Spec spec_;
  mutable Totals *totals_;
  const bool deleteAfterwards_;
};

class Source {
public:
  Source(const std::string &from,
         const std::string &to,
         unsigned int streams,
         unsigned int orbitBitsPerLS,
         bool hasBroker,
         int inotify_fd,
         unsigned int run,
         Totals &totals)
      : from_(from),
        to_(to),
        hasBroker_(hasBroker),
        inotify_fd_(inotify_fd),
        streams_(streams),
        orbitBitsPerLS_(orbitBitsPerLS),
        run_(run),
        pid_(hasBroker ? getpid() : 0),
        totals_(&totals) {
    char buff[256];
    if (gethostname(buff, sizeof(buff)) == 0) {
      hostname_ = buff;
    } else {
      hostname_ = "unknown";
    }
  }

  Token operator()(bool &stop) const {
    Token ret;
    if (workQueue_.empty()) {
      if (!readMessages()) {
        stop = true;
      }
    }
    if (!workQueue_.empty()) {
      ret = workQueue_.front();
      workQueue_.pop_front();
    }
    totals_->maybe_start();
    return ret;
  }
  Token operator()(tbb::flow_control &fc) const {
    bool stop = false;
    Token ret = operator()(stop);
    if (stop)
      fc.stop();
    return ret;
  }

private:
  static const unsigned int EVENT_SIZE = sizeof(struct inotify_event);
  static const unsigned int BUF_LEN = 1024 * (EVENT_SIZE + 16);
  const std::string from_, to_;
  std::string hostname_;
  bool hasBroker_;
  const int inotify_fd_;
  unsigned int streams_, orbitBitsPerLS_;
  unsigned int run_, pid_;
  mutable Totals *totals_;

  bool parseUnpackedFilename(const std::string &fname,
                             unsigned int &orbit,
                             unsigned int &streamid,
                             std::string &fout) const {
    // in the form run000037_ls0001_index000513[_stream00].raw
    if (fname.length() < 5)
      return false;
    std::string work = fname.substr(0, fname.length() - 4);  // remove the ".raw";
    auto pos = work.rfind('_');
    if (pos == std::string::npos || (work.substr(pos, 7) != "_stream")) {
      return false;
    }
    streamid = std::atoi(work.substr(pos + 7).c_str());
    work = work.substr(0, pos);
    pos = work.rfind('_');
    if (pos == std::string::npos || work.substr(pos, 6) != "_index") {
      return false;
    }
    unsigned int index = std::atoi(work.substr(pos + 6).c_str());
    auto pos2 = work.rfind('_', pos - 1);
    if (pos2 == std::string::npos || work.substr(pos2, 3) != "_ls") {
      return false;
    }
    unsigned int ls = std::atoi(work.substr(pos2 + 3, pos).c_str());
    orbit = (std::max(ls - 1, 0u) << orbitBitsPerLS_) + index;
    fout = work;
    /*printf("parseUnpF('%s') --> orbit %u, ls %u, streamid %u, fout '%s'\n",
           work.c_str(),
           orbit,
           ls,
           streamid,
           fout.c_str());*/
    return true;
  }
  struct DemuxQueueItem {
    unsigned int orbit;
    std::vector<std::string> streams;
    std::string fout;
    DemuxQueueItem(unsigned int orbit_, unsigned int nstreams, const std::string &fout_)
        : orbit(orbit_), streams(nstreams), fout(fout_) {}
  };
  mutable std::list<DemuxQueueItem> demuxQueue_;
  mutable std::deque<Token> workQueue_;

  bool takeFile(const std::string &in, std::string &newname) const {
    newname = in + ".taken." + hostname_;
    std::error_code ec;
    std::filesystem::rename(in, newname, ec);
    if (!ec) {
      if (std::filesystem::exists(newname, ec)) {
        if (!ec) {
          return true;
        }
      }
      printf("ERROR: missing %s after rename, error %d (%s)\n", newname.c_str(), ec.value(), ec.message().c_str());
    } else {
      printf("ERROR: couldn't take %s, error %d (%s)\n", in.c_str(), ec.value(), ec.message().c_str());
    }
    return false;
  }

  void maybeAddSingle(const std::string &fname, std::string &in) const {
    std::string newname;
    if (!takeFile(in, newname))
      return;
    // strip ".raw" postfix
    std::string out = to_ + "/" + fname.substr(0, fname.length() - 4);
    // check for duplicates
    bool found = false;
    for (const auto &el : workQueue_) {
      if (el.output == out) {
        found = true;
        break;
      }
    }
    if (!found)
      workQueue_.emplace_back(newname, out);
  }

  void maybeAddStream(const std::string &fname, std::string &in) const {
    unsigned int orbit;
    unsigned int streamid;
    std::string fout;
    if (!parseUnpackedFilename(fname, orbit, streamid, fout))
      return;
    bool found = false;
    for (auto it = demuxQueue_.begin(), ed = demuxQueue_.end(); it != ed; ++it) {
      if (it->orbit != orbit)
        continue;
      assert(streamid < streams_ && it->streams.size() == streams_);
      //printf("Found demuxQueue record for orbit %u\n", orbit);
      //or (unsigned int i = 0; i < streams_; ++i) {
      //  printf("  streamid[%u] = '%s'\n", i, it->streams[i].c_str());
      //}
      if (it->streams[streamid].empty()) {
        it->streams[streamid] = in;
        bool complete = true;
        for (const auto &t : it->streams) {
          if (t.empty())
            complete = false;
        }
        if (complete) {
          //printf("demuxQueue record for orbit %u has all %u streams available.\n", orbit, streams_);
          bool taken = true;
          for (auto &i : it->streams) {
            std::string newname;
            if (!takeFile(i, newname)) {
              taken = false;
              break;
            }
            i = newname;
          }
          if (!taken)
            continue;
          workQueue_.emplace_back(it->streams, to_ + "/" + it->fout);
          demuxQueue_.erase(it);
        }
      } else {
        if (it->streams[streamid] != in) {
          printf("ERROR, mismatch files for orbit %d, streamid %d: %s vs %s\n",
                 orbit,
                 streamid,
                 it->streams[streamid].c_str(),
                 in.c_str());
          abort();
        }
      }
      found = true;
      break;
    }
    if (!found) {
      //printf("Creating new demuxQueue record for orbit %u, with streamid %u\n", orbit, streamid);
      auto &el = demuxQueue_.emplace_back(orbit, streams_, fout);
      el.streams[streamid] = in;
    }
  }

  void waitAndAddAll(const std::string &fname) const {
    // no need to take the first file since the file broker already arbitrates conflicts
    auto match = fname.find("_stream00.raw");
    assert(match != std::string::npos);
    auto fnameBase = fname.substr(0, match);
    std::string out = to_ + "/" + fnameBase;
    // check for duplicates
    bool found = false;
    for (const auto &el : workQueue_) {
      if (el.output == out) {
        found = true;
        break;
      }
    }
    if (found)
      return;  // not an error
    std::vector<std::string> items(1, from_ + "/" + fname);
    char buff[16];
    for (unsigned int s = 1; s < streams_; ++s) {
      snprintf(buff, sizeof(buff), "_stream%02u.raw", s);
      items.emplace_back(from_ + "/" + fnameBase + buff);
      std::filesystem::path pin(items.back());
      for (unsigned int iwait = 0; iwait < 600; ++iwait) {
        if (std::filesystem::exists(pin))
          break;
        std::this_thread::sleep_for(std::chrono::milliseconds(25));
        if (iwait % 40 == 0)
          std::cout << "Waiting for " << items.back() << ", not yet available" << std::endl;
      }
    }
    if (items.size() == streams_) {
      workQueue_.emplace_back(items, out);
    } else {
      std::cout << "ERROR: timeout waiting for some streams of " << fname << std::endl;
    }
  }

  bool readMessages() const { return hasBroker_ ? pollBroker() : readMessagesInotify(); }

  bool readMessagesInotify() const {
    char buffer[BUF_LEN];

    pollfd poller{inotify_fd_, POLLIN, 0};
    bool foundFiles = totals_->started.load();
    unsigned int timeout_ms = (foundFiles ? 5 : 5 * 60) * 1000;

    for (;;) {
      int pollret = poll(&poller, 1, timeout_ms);
      if (pollret == 0) {
        printf("Stop monitoring after timeout of %.1fs elapsed without %s files\n",
               timeout_ms / 1000.,
               foundFiles ? "any new" : "any");
        return false;
      } else if (pollret < 0) {
        if (errno == EINTR)
          continue;
        perror("Error in polling inotify fd");
        return false;
      }
      int length = read(inotify_fd_, buffer, BUF_LEN);

      if (length < 0) {
        perror("Error in reading inotify fd");
        return false;
      }

      for (int i = 0; i < length; i += EVENT_SIZE + reinterpret_cast<inotify_event *>(&buffer[i])->len) {
        struct inotify_event *event = reinterpret_cast<inotify_event *>(&buffer[i]);
        if (event->len) {
          if (event->mask & (IN_CLOSE_WRITE | IN_MOVED_TO)) {
            if (!(event->mask & IN_ISDIR)) {
              std::string fname = event->name;
              if (fname.length() > 4 && fname.substr(fname.length() - 4) == ".raw") {
                std::string in(from_ + "/" + fname);
                if (std::filesystem::exists(in)) {
                  if (streams_ == 0) {
                    maybeAddSingle(fname, in);
                  } else {
                    maybeAddStream(fname, in);
                  }
                }
              }
            }
          }
        }
      }

      if (length > 0)
        return true;
    }

    return false;  // unreachable
  }

  bool pollBroker() const {
    std::string request = "GET /popfile?runnumber=" + std::to_string(run_) + "&pid=" + std::to_string(pid_) +
                          " HTTP/1.1\r\nConnection: Keep-Alive\r\n\r\n";
    char buff[2048];
    unsigned int iwait = 0;
    for (;;) {
      int ret = send(inotify_fd_, request.c_str(), request.size(), 0);
      if (ret < 0) {
        perror("Error sending to broker");
        return false;
      }
      int nb = recv(inotify_fd_, buff, sizeof(buff), 0);
      if (nb < 0) {
        perror("Error receiving from broker");
        return false;
      }
      std::string_view fullReply(buff, nb);
      auto headerEnd = fullReply.find("\r\n\r\n");
      if (headerEnd == std::string_view::npos) {
        std::cout << "Error when parsing full reply:\n------\n" << fullReply << "\n-----" << std::endl;
        return false;
      }
      auto reply = fullReply.substr(headerEnd + 4);
      auto getarg = [&](std::string_view key) {
        auto item = reply.find("\n" + std::string(key) + "=");
        auto step = key.size() + 2;
        if (item != std::string_view::npos) {
          auto end = reply.find_first_of("\r\n", item + step);
          return reply.substr(item + step, end - item - step);
        } else {
          return std::string_view();
        }
      };
      auto state = getarg("state"), file = getarg("file");
      if (state.empty()) {
        std::cout << "Error when parsing reply:\n------\n" << reply << "\n-----" << std::endl;
        return false;
      }
      if (state == "STARTING") {
        if ((++iwait) % 10 == 0)
          std::cout << "Run is still starting" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        continue;
      } else if (state == "READY" || state == "EOLS") {
        if (file.empty()) {
          if ((++iwait) % 40 == 0)
            std::cout << "Started but no file available currently, will retry soon" << std::endl;
          std::this_thread::sleep_for(std::chrono::milliseconds(25));
          continue;
        }
        iwait = 0;
        std::string fname = std::string(file) + ".raw";
        std::string in(from_ + "/" + fname);
        if (std::filesystem::exists(in)) {
          if (streams_ == 0) {
            maybeAddSingle(fname, in);
          } else {
            waitAndAddAll(fname);
          }
        }
        return true;
      } else if (state == "EOR") {
        std::cout << "End of run" << std::endl;
        return false;
      } else {
        std::cout << "Bad state '" << state << "' when parsing reply:\n------\n" << reply << "\n-----" << std::endl;
        return false;
      }
    }
  }
};

std::pair<int, int> initInotify(const std::string &from) {
  int fd = inotify_init();
  if (fd < 0) {
    perror("inotify_init");
    return std::make_pair(fd, 0);
  }

  int wd = inotify_add_watch(fd, from.c_str(), IN_CLOSE_WRITE | IN_MOVED_TO);
  printf("Watching %s for new files\n", from.c_str());
  return std::make_pair(fd, wd);
}

int openBrokerConnection(const std::string &broker) {
  auto pos = broker.find(":");
  auto addr = broker.substr(0, pos);
  unsigned int port = std::stoul(broker.substr(pos + 1));
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("ERROR opening socket");
    return -1;
  }
  struct sockaddr_in serv_addr;
  bzero((char *)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  serv_addr.sin_addr.s_addr = inet_addr(addr.c_str());
  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))) {
    perror("ERROR on connect");
    return -2;
  }
  std::cout << "Connected to fileBroker on " << addr << ", port " << port << std::endl;
  return sockfd;
}

int singleCoreLiveUnpacker(Totals &totals,
                           const UnpackMaker::Spec &unpackerSpec,
                           const std::string &broker,
                           const std::string &from,
                           const std::string &to,
                           unsigned int streams,
                           unsigned int orbitBitsPerLS,
                           unsigned int runNumber,
                           bool deleteAfterwards) {
  bool hasBroker = !broker.empty();
  int sourcefd = -1, sourcewd = 0;
  if (!broker.empty()) {
    sourcefd = openBrokerConnection(broker);
  } else {
    auto fdwd = initInotify(from);
    sourcefd = fdwd.first;
    sourcewd = fdwd.second;
  }
  if (sourcefd < 0) {
    return -1;
  }

  auto src = Source(from, to, streams, orbitBitsPerLS, hasBroker, sourcefd, runNumber, totals);
  auto dest = Executor(unpackerSpec, totals, deleteAfterwards);
  bool stop = false;
  while (!stop) {
    dest(src(stop));
  }

  if (!hasBroker) {
    inotify_rm_watch(sourcefd, sourcewd);
  }
  close(sourcefd);

  return 0;
}

int tbbLiveUnpacker(Totals &totals,
                    unsigned int threads,
                    const UnpackMaker::Spec &unpackerSpec,
                    const std::string &broker,
                    const std::string &from,
                    const std::string &to,
                    unsigned int streams,
                    unsigned int orbitBitsPerLS,
                    unsigned int runNumber,
                    bool deleteAfterwards) {
  bool hasBroker = !broker.empty();
  int sourcefd = -1, sourcewd = 0;
  if (!broker.empty()) {
    sourcefd = openBrokerConnection(broker);
  } else {
    auto fdwd = initInotify(from);
    sourcefd = fdwd.first;
    sourcewd = fdwd.second;
  }
  if (sourcefd < 0) {
    return -1;
  }

#ifdef USE_ROOT
  ROOT::EnableThreadSafety();
#endif

  auto head = tbb::make_filter<void, Token>(
      tbb::filter::serial_in_order, Source(from, to, streams, orbitBitsPerLS, hasBroker, sourcefd, runNumber, totals));
  auto tail = tbb::make_filter<Token, void>((threads == 0 ? tbb::filter::serial_in_order : tbb::filter::parallel),
                                            Executor(unpackerSpec, totals, deleteAfterwards));
  tbb::parallel_pipeline(std::max(1u, threads), head & tail);

  if (!hasBroker) {
    inotify_rm_watch(sourcefd, sourcewd);
  }
  close(sourcefd);

  return 0;
}

int main(int argc, char **argv) {
  if (argc < 6) {
    usage();
    return 1;
  }

  std::string compressionMethod = "none";
  int compressionLevel = 0, threads = -1;
  unsigned int streams = 0, orbitBitsPerLS = 18, runNumber = 1;
  bool cmsswHeaders = false;
  bool deleteAfterwards = false;
  UnpackerBase::Spec::Options options;
  std::string prometheusExport;
  while (1) {
    static struct option long_options[] = {{"help", no_argument, nullptr, 'h'},
                                           {"batchsize", required_argument, nullptr, 'b'},
                                           {"threads", required_argument, nullptr, 'j'},
                                           {"iothreads", required_argument, nullptr, 'J'},
                                           {"demux", required_argument, nullptr, 'd'},
                                           {"runNumber", required_argument, nullptr, 'r'},
                                           {"orbitBitsPerLS", required_argument, nullptr, 1},
                                           {"compression", required_argument, nullptr, 'z'},
                                           {"prometheus", required_argument, nullptr, 3},
                                           {"cmssw", no_argument, nullptr, 2},
                                           {"delete", no_argument, nullptr, 'D'},
                                           {nullptr, 0, nullptr, 0}};
    int option_index = 0;
    int optc = getopt_long(argc, argv, "hz:j:d:DM:r:", long_options, &option_index);
    if (optc == -1)
      break;

    switch (optc) {
      case 'h':
        usage();
        return 0;
      case 'b':
        options["batchsize"] = optarg;
        break;
      case 'z': {
        compressionMethod = std::string(optarg);
        auto pos = compressionMethod.find(",");
        if (pos != std::string::npos) {
          compressionLevel = std::atoi(compressionMethod.substr(pos + 1).c_str());
          compressionMethod = compressionMethod.substr(0, pos);
        } else {
          compressionLevel = 4;
        }
      } break;
      case 'j':
        threads = std::atoi(optarg);
        break;
      case 'J':
        options["iothreads"] = optarg;
        break;
      case 'd':
        streams = std::atoi(optarg);
        break;
      case 'r':
        runNumber = std::atol(optarg);
        break;
      case 1:
        orbitBitsPerLS = std::atoi(optarg);
        assert(orbitBitsPerLS > 0);
        break;
      case 2:
        cmsswHeaders = true;
        break;
      case 3:
        prometheusExport = std::string(optarg);
        break;
      case 'D':
        deleteAfterwards = true;
        break;
      default:
        usage();
        return 1;
    }
  }

  int iarg = optind, narg = argc - optind;
  if (narg < 5) {
    usage();
    return 1;
  }
  UnpackerBase::Spec spec(argv[iarg], argv[iarg + 1], argv[iarg + 2], options);
  spec.setCMSSWHeaders(cmsswHeaders);
  spec.setCompression(compressionMethod, compressionLevel);
  std::string broker;
  std::string to = argv[argc - 1];  // always last argument
  std::vector<std::string> froms;
  for (int ia = iarg + 3; ia < argc - 1; ++ia) {
    froms.emplace_back(argv[ia]);
    if (froms.back().find(':') != std::string::npos) {
      if (!broker.empty()) {
        printf("Multiple brokers specified: %s, %s\n", broker.c_str(), froms.back().c_str());
        return 2;
      }
      broker = froms.back();
      froms.pop_back();
      printf("Using file broker %s\n", broker.c_str());
    } else {
      printf("Reading from %s\n", froms.back().c_str());
      assert(std::filesystem::is_directory(std::filesystem::path(froms.back())));
    }
  }
  std::string from = froms.front();
  printf(
      "Will run %s %s format %s from %s to %s\n", argv[iarg], argv[iarg + 1], argv[iarg + 2], from.c_str(), to.c_str());
  std::filesystem::path pto = to;
  if (!std::filesystem::is_directory(pto))
    std::filesystem::create_directories(pto);
  assert(std::filesystem::is_directory(pto));

  UnpackMaker::make(spec);  // trigger creation of one unpacker, to test if options are valid
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
  auto prometheusRegistry = std::make_shared<prometheus::Registry>();
  Totals totals(*prometheusRegistry);
  std::unique_ptr<prometheus::Exposer> prometheusExposer;
  if (!prometheusExport.empty()) {
    CPUCounter::start("unpacker", *prometheusRegistry);
    DirGauge::start("unpacker_input_directory", from, false, *prometheusRegistry);
    DirGauge::start("unpacker_output_directory", to, false, *prometheusRegistry);
    prometheusExposer = std::make_unique<prometheus::Exposer>(prometheusExport);
    prometheusExposer->RegisterCollectable(prometheusRegistry);
    printf("Exporting prometheus counters on http://%s/metrics\n", prometheusExport.c_str());
  }
#else
  if (!prometheusExport.empty()) {
    printf("This version was not compiled with Prometheus support.\n");
    return 1;
  }
  Totals totals;
#endif
  if (threads == -1) {
    return singleCoreLiveUnpacker(totals, spec, broker, from, to, streams, orbitBitsPerLS, runNumber, deleteAfterwards);
  } else if (threads >= 0) {
    return tbbLiveUnpacker(
        totals, threads, spec, broker, from, to, streams, orbitBitsPerLS, runNumber, deleteAfterwards);
  }
}