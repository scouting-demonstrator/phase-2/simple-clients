#ifndef p2_clients_UnpackMaker_h
#define p2_clients_UnpackMaker_h
#include "UnpackerBase.h"
#include <memory>
#include <string_view>

namespace UnpackMaker {
  typedef UnpackerBase::Spec Spec;
  std::unique_ptr<UnpackerBase> make(const Spec &spec);
  inline std::unique_ptr<UnpackerBase> make(const std::string &object,  // puppi or tkmu
                                            const std::string &kind,    // ttree or rntuple
                                            const std::string &format,
                                            const Spec::Options &options = {}) {
    return make(Spec(object, kind, format, options));
  }
  bool is_output_fname(const std::string &fname);
}  // namespace UnpackMaker

#endif