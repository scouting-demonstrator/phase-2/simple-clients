#ifndef p2_clients_RNTupleUnpackerCollInt_h
#define p2_clients_RNTupleUnpackerCollInt_h
#include "RNTupleUnpackerBase.h"

#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 32, 00)
  #include <ROOT/RNTupleCollectionWriter.hxx>
#endif

class RNTupleUnpackerCollInt : public RNTupleUnpackerBase {
public:
  RNTupleUnpackerCollInt(const Spec &spec) : RNTupleUnpackerBase(spec) {}
  ~RNTupleUnpackerCollInt() override {}

  void addMyOutput(ROOT::Experimental::RNTupleModel *model) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  struct Data {
    std::shared_ptr<uint16_t> p_pt;
    std::shared_ptr<int16_t> p_eta, p_phi;
    std::shared_ptr<uint8_t> p_pid, p_quality;
    std::shared_ptr<int16_t> p_z0;
    std::shared_ptr<int8_t> p_dxy;
    std::shared_ptr<uint16_t> p_wpuppi;
#if ROOT_VERSION_CODE < ROOT_VERSION(6, 32, 00)
    std::shared_ptr<ROOT::Experimental::RCollectionNTupleWriter> subwriter;
#else
    std::shared_ptr<ROOT::Experimental::RNTupleCollectionWriter> subwriter;
#endif
  };

  Data data_;
};

#endif