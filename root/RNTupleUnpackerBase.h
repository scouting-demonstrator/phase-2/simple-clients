#ifndef p2_clients_RNTupleUnpackerBase_h
#define p2_clients_RNTupleUnpackerBase_h
#include <chrono>
#include <TROOT.h>
#include <TFile.h>
#include <ROOT/RNTuple.hxx>
#include <ROOT/RNTupleModel.hxx>
#include <ROOT/RNTupleUtil.hxx>
#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 32, 00)
  #include <ROOT/RNTupleWriter.hxx>
  #include <ROOT/RNTupleCollectionWriter.hxx>
#endif
#include "../UnpackerBase.h"
#include "../unpack.h"

class RNTupleUnpackerBase : public UnpackerBase {
public:
  RNTupleUnpackerBase(const Spec &spec)
      : UnpackerBase(spec), compression_(parseCompression(spec.compressionAlgo(), spec.compressionLevel())) {
    initMT(spec.numThreads());
    if (compression_)
      printf("Enabled compression with %s, level %d\n", spec.compressionAlgo().c_str(), spec.compressionLevel());
  }

  ~RNTupleUnpackerBase() override {}

  static void initMT(unsigned int threads);
  static int parseCompression(const std::string &algo, unsigned int level);

  void bookOutput(const std::string &out) override {
    auto model = modelBase();
    addMyOutput(model.get());
    bookBase(out, std::move(model));
  }

  virtual void addMyOutput(ROOT::Experimental::RNTupleModel *model) = 0;

  unsigned long int closeOutput() override;

  std::string outputExtension() override { return "root"; }

protected:
  int compression_;
  std::string fout_;
  std::unique_ptr<ROOT::Experimental::RNTupleWriter> writer_;
  struct DataBase {
    std::shared_ptr<uint16_t> p_run, p_bx;
    std::shared_ptr<uint32_t> p_orbit;
    std::shared_ptr<bool> p_good;
  };
  DataBase dataBase_;

  std::unique_ptr<ROOT::Experimental::RNTupleModel> modelBase() {
    auto model = ROOT::Experimental::RNTupleModel::Create();
    dataBase_.p_run = model->MakeField<uint16_t>("run");
    dataBase_.p_orbit = model->MakeField<uint32_t>("orbit");
    dataBase_.p_bx = model->MakeField<uint16_t>("bx");
    return model;
  }

  void bookBase(const std::string &fout, std::unique_ptr<ROOT::Experimental::RNTupleModel> model);
  static std::shared_ptr<float> makeFloatField(ROOT::Experimental::RNTupleModel &model,
                                               std::string_view name,
                                               bool float16 = false);
  static std::shared_ptr<std::vector<float>> makeVFloatField(ROOT::Experimental::RNTupleModel &model,
                                                             std::string_view name,
                                                             bool float16 = false);
};

#endif