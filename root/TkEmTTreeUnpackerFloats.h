#ifndef p2_clients_TkEmTTreeUnpackerFloats_h
#define p2_clients_TkEmTTreeUnpackerFloats_h
#include "TTreeUnpackerBase.h"

class TkEmTTreeUnpackerFloats : public TTreeUnpackerBase {
public:
  TkEmTTreeUnpackerFloats(const Spec &spec) : TTreeUnpackerBase(spec), floatType_(spec.format) {}
  ~TkEmTTreeUnpackerFloats() override {}

  void addMyOutput(TTree *tree) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  std::string floatType_;

  struct Data {
    uint16_t nem;
    float pt[255];
    float eta[255];
    float phi[255];
    uint8_t quality[255];
    float isolation[255];
  } data_;
  struct DataEle : public Data {
    int8_t charge[255];
    float z0[255];
  } dataEle_;
};

#endif