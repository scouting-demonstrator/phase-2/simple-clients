CC = c++
CCFLAGS = -W -Wall -Wno-unused-variable -Ofast -march=x86-64 -msse4.2 -mavx2 -mfma -mf16c   $(shell root-config --cflags)
LIBS = -lstdc++ $(shell root-config --ldflags --libs) -lROOTNTuple
LIVE_CCFLAGS :=
LIVE_CLIBS :=
DATAFILE ?= data/Puppi.dump
TKMUDATAFILE ?= data/TkMuons_TM18.dump
OUTDIR ?= /run/user/$$UID
ROOTVERSION = $(shell root-config --version | sed 's/\.//g')

ifdef VDT_INCLUDE_DIR
	CCFLAGS += -I$(VDT_INCLUDE_DIR)
endif
ifdef VDT_LIBRARY
	LIBS += -L$(dir $(VDT_LIBRARY))
endif

INSTALL_PREFIX ?= /usr/local

.PHONY: clean install format run_tests run_test_speed run_test_comp run_test_multi run_test_tkmu 

TARGETS := librootUnpacker.so
all: $(TARGETS)
	@cd rdf_analysis && $(MAKE) all

vpath %.so . ..
BASE_INC := ../unpack.h  ../UnpackerBase.h
BASE_LIB := libunpackerBase.so
$(BASE_LIB): ../UnpackerBase.cc $(BASE_INC)
	cd .. && $(MAKE) $@
TREE_UNPACKER_SRC := $(wildcard *TTreeUnpacker*.cc)
TREE_UNPACKER_INC := $(TREE_UNPACKER_SRC:.cc=.h)
TREE_UNPACKER_OBJ := $(TREE_UNPACKER_SRC:.cc=.o)
$(TREE_UNPACKER_OBJ): %.o: %.cc %.h TTreeUnpackerBase.h $(BASE_INC)
	$(CC) -fPIC $(CCFLAGS) -c $< -o $@

RNTUPLE_UNPACKER_SRC := $(wildcard *RNTupleUnpacker*.cc)
RNTUPLE_UNPACKER_INC := $(RNTUPLE_UNPACKER_SRC:.cc=.h)
RNTUPLE_UNPACKER_OBJ := $(RNTUPLE_UNPACKER_SRC:.cc=.o)
$(RNTUPLE_UNPACKER_OBJ): %.o: %.cc %.h RNTupleUnpackerBase.h $(BASE_INC) 
	$(CC) -fPIC $(CCFLAGS) -c $< -o $@

ALLOBJ := RootUnpackMaker.o $(RNTUPLE_UNPACKER_OBJ) $(TREE_UNPACKER_OBJ)

RootUnpackMaker.o : RootUnpackMaker.cc RootUnpackMaker.h  $(TREE_UNPACKER_INC) $(RNTUPLE_UNPACKER_INC) $(BASE_INC)
	$(CC) -fPIC $(CCFLAGS) -c $< -o $@

librootUnpacker.so: $(ALLOBJ) libunpackerBase.so
	$(CC) $(CCFLAGS) $(ALLOBJ) -shared -fPIC $(LIBS) -L..  -lunpackerBase -o $@

format:
	@clang-format -i *.h *.cc
	@cd rdf_analysis && $(MAKE) format

clean:
	@rm $(TARGETS) *.root *.o *.so *.raw 2> /dev/null || true
	@cd rdf_analysis && $(MAKE) clean

install: $(TARGETS)
	@install -v librootUnpacker.so $(INSTALL_PREFIX)/lib64/
	@cd rdf_analysis && $(MAKE) install

../p2scout-unpacker:
	@cd .. && $(MAKE) p2scout-unpacker

run_tests: ../p2scout-unpacker
	@for T in float float24 int raw64; do  \
		../p2scout-unpacker puppi ttree $$T data/Puppi.dump out.root; \
	done
	@for T in floats floats16 coll_float coll_float16 ints coll_int raw64; do  \
		[ $(ROOTVERSION) -lt 63200 ] && [ $$T = float16 -o $$T = coll_float16 ] && continue; \
		../p2scout-unpacker puppi rntuple $$T data/Puppi.dump out.root; \
	done
	@for T in float float24 int raw96; do  \
		../p2scout-unpacker tkmu ttree $$T data/TkMuons_TM18.dump out.root; \
	done
	@for T in floats floats16 coll_float coll_float16 raw96; do  \
		[ $(ROOTVERSION) -lt 63200 ] && [ $$T = float16 -o $$T = coll_float16 ] && continue; \
		../p2scout-unpacker tkmu rntuple $$T data/TkMuons_TM18.dump out.root; \
	done
	@for T in float float24 raw96; do  \
		../p2scout-unpacker tkem ttree $$T data/TkEmAndEle_TM6.dump out.root; \
	done
	@for T in floats floats16 coll_float coll_float16 raw96; do  \
		[ $(ROOTVERSION) -lt 63200 ] && [ $$T = float16 -o $$T = coll_float16 ] && continue; \
		../p2scout-unpacker tkem rntuple $$T data/TkEmAndEle_TM6.dump out.root; \
	done

run_test_composite: ../p2scout-unpacker
	../p2scout-generator CMSSW data/Puppi.dump -T 1 --orbits 5 puppi.tm1.raw > /dev/null
	../p2scout-unpacker --cmssw puppi ttree float puppi.tm1.raw out.ref.root
	../p2scout-unpacker --cmssw composite ttree puppi-float puppi.tm1.raw out.root
	../p2scout-generator CMSSW data/Puppi.dump -n 6 -T 6 --orbits 5 puppi.tm6.%d.raw > /dev/null
	../p2scout-unpacker --cmssw puppi ttree float puppi.tm6.{0,1,2,3,4,5}.raw out6.ref.root
	../p2scout-unpacker --cmssw composite ttree puppi-float-6 puppi.tm6.{0,1,2,3,4,5}.raw out6.root
	../p2scout-unpacker --cmssw puppi ttree int puppi.tm6.{0,1,2,3,4,5}.raw out6.ref.root
	../p2scout-unpacker --cmssw composite ttree puppi-int-6 puppi.tm6.{0,1,2,3,4,5}.raw out6.root
	../p2scout-unpacker --cmssw puppi rntuple floats puppi.tm6.{0,1,2,3,4,5}.raw out6.ref.root
	../p2scout-unpacker --cmssw composite rntuple puppi-floats-6 puppi.tm6.{0,1,2,3,4,5}.raw out6.root
	../p2scout-unpacker --cmssw puppi rntuple coll_float puppi.tm6.{0,1,2,3,4,5}.raw out6.ref.root
	../p2scout-unpacker --cmssw composite rntuple puppi-coll_float-6 puppi.tm6.{0,1,2,3,4,5}.raw out6.root
	../p2scout-generator Native64 data/Puppi.dump -n 6 -T 6 --orbits 5 puppi64.tm6.%d.raw > /dev/null
	../p2scout-unpacker  puppi ttree float puppi64.tm6.{0,1,2,3,4,5}.raw out6.ref.root
	../p2scout-unpacker  composite ttree puppi-float-6 puppi64.tm6.{0,1,2,3,4,5}.raw out6.root
	../p2scout-generator CMSSW data/TkMuons_TM18.dump -n 18 -T 18 --orbits 5 tkmu.tm18.%d.raw > /dev/null
	../p2scout-unpacker --cmssw tkmu ttree float tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out18.ref.root
	../p2scout-unpacker --cmssw composite ttree tkmu-float-18 tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out18.root
	../p2scout-unpacker --cmssw tkmu rntuple floats tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out18.ref.root
	../p2scout-unpacker --cmssw composite rntuple tkmu-floats-18 tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out18.root
	../p2scout-unpacker --cmssw composite ttree puppi-float-6,tkmu-float-18 puppi.tm6.{0,1,2,3,4,5}.raw  tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out24.root
	../p2scout-unpacker --cmssw composite rntuple puppi-floats-6,tkmu-floats-18 puppi.tm6.{0,1,2,3,4,5}.raw  tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out24.root
	../p2scout-unpacker --cmssw composite rntuple puppi-coll_float-6,tkmu-coll_float-18 puppi.tm6.{0,1,2,3,4,5}.raw  tkmu.tm18.{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17}.raw out24.root

run_test_speed: ../p2scout-unpacker 
	@echo "==== No output ===="
	@../p2scout-unpacker puppi ttree raw64 $(DATAFILE)
	@../p2scout-unpacker puppi ttree int $(DATAFILE)
	@../p2scout-unpacker puppi ttree float $(DATAFILE)
	@echo "==== With output ===="
	@../p2scout-unpacker puppi ttree raw64 $(DATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker puppi ttree int $(DATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker puppi ttree float $(DATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker puppi ttree float24 $(DATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple raw64 $(DATAFILE)  $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple ints $(DATAFILE)  $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple coll_int $(DATAFILE)  $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple floats $(DATAFILE)  $(OUTDIR)/out.root
	@[ $(ROOTVERSION) -ge 63200 ] && ../p2scout-unpacker puppi rntuple floats16 $(DATAFILE)  $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple coll_float $(DATAFILE)  $(OUTDIR)/out.root 
	@[ $(ROOTVERSION) -ge 63200 ] && ../p2scout-unpacker puppi rntuple coll_float16 $(DATAFILE)  $(OUTDIR)/out.root


run_test_multi: ../p2scout-unpacker
	@for I in $$(seq 1 6); do cp -v $(DATAFILE) $(OUTDIR)/in$${I}.dump; done
	@../p2scout-unpacker puppi ttree float $(OUTDIR)/in{1,2,3,4,5,6}.dump;
	@../p2scout-unpacker puppi ttree float $(OUTDIR)/in{1,2,3,4,5,6}.dump;
	@../p2scout-unpacker puppi rntuple coll_float $(OUTDIR)/in{1,2,3,4,5,6}.dump
	@../p2scout-unpacker puppi ttree float  $(OUTDIR)/in{1,2,3,4,5,6}.dump $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple floats $(OUTDIR)/in{1,2,3,4,5,6}.dump $(OUTDIR)/out.root
	@../p2scout-unpacker puppi rntuple coll_float $(OUTDIR)/in{1,2,3,4,5,6}.dump $(OUTDIR)/out.root

run_test_comp: ../p2scout-unpacker
	@echo "==== With LZ4 output ===="
	@../p2scout-unpacker puppi ttree int $(DATAFILE) $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi rntuple ints $(DATAFILE)  $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi rntuple coll_int $(DATAFILE)  $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi ttree float $(DATAFILE) $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi ttree float24 $(DATAFILE) $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi rntuple floats $(DATAFILE)  $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker puppi rntuple coll_float $(DATAFILE)  $(OUTDIR)/out.root -j 6 -z lz4,4
	@echo "==== With ZSTD output ===="
	@../p2scout-unpacker puppi ttree float $(DATAFILE) $(OUTDIR)/out.root -j 6 -z zstd,5
	@../p2scout-unpacker puppi rntuple coll_float $(DATAFILE)  $(OUTDIR)/out.root -j 6 -z zstd,5

run_test_tkmu: ../p2scout-unpacker
	@echo "==== With uncompressed output  ===="
	@../p2scout-unpacker tkmu ttree int $(TKMUDATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker tkmu ttree float $(TKMUDATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker tkmu rntuple floats $(TKMUDATAFILE) $(OUTDIR)/out.root
	@[ $(ROOTVERSION) -ge 63200 ] && ../p2scout-unpacker tkmu rntuple floats16 $(TKMUDATAFILE) $(OUTDIR)/out.root
	@../p2scout-unpacker tkmu rntuple coll_float $(TKMUDATAFILE) $(OUTDIR)/out.root
	@[ $(ROOTVERSION) -ge 63200 ] && ../p2scout-unpacker tkmu rntuple coll_float16 $(TKMUDATAFILE) $(OUTDIR)/out.root
	@echo "==== With LZ4 output ===="
	@../p2scout-unpacker tkmu ttree int $(TKMUDATAFILE) $(OUTDIR)/out.root -j 6 -z lz4,4
	@../p2scout-unpacker tkmu ttree float $(TKMUDATAFILE) $(OUTDIR)/out.root -j 6 -z lz4,4