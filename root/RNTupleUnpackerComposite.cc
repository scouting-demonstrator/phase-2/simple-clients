#include "RNTupleUnpackerComposite.h"
#include <cassert>
#include <typeinfo>

RNTupleUnpackerComposite::RNTupleUnpackerComposite(const Spec &spec) : RNTupleUnpackerBase(spec) {
  for (auto &src : sources_) {
    if (!dynamic_cast<RNTupleUnpackerBase *>(src.unpacker.get())) {
      throw std::logic_error("RNTupleUnpackerComposite has a child which is not a RNTupleUnpackerBase but rather a " +
                             std::string(typeid(*src.unpacker).name()));
    }
  }
}

void RNTupleUnpackerComposite::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("good");
  for (auto &src : sources_) {
    (dynamic_cast<RNTupleUnpackerBase &>(*src.unpacker)).addMyOutput(model);
  }
}

void RNTupleUnpackerComposite::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  assert(nwords == 0 && words == nullptr && commit);
  *dataBase_.p_run = run;
  *dataBase_.p_orbit = orbit;
  *dataBase_.p_bx = bx;
  *dataBase_.p_good = good;
  if (writer_)
    writer_->Fill();
}
