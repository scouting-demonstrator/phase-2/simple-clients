#include "RNTupleUnpackerCollFloat.h"
#include "../unpack.h"

void RNTupleUnpackerCollFloat::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("goodPuppi");
  auto submodel = ROOT::Experimental::RNTupleModel::Create();
  data_.p_pt = makeFloatField(*submodel, "pt", float16_);
  data_.p_eta = makeFloatField(*submodel, "eta", float16_);
  data_.p_phi = makeFloatField(*submodel, "phi", float16_);
  data_.p_pdgid = submodel->MakeField<short int>("pdgId");
  data_.p_z0 = makeFloatField(*submodel, "z0", float16_);
  data_.p_dxy = makeFloatField(*submodel, "dxy", float16_);
  data_.p_wpuppi = makeFloatField(*submodel, "wpuppi", float16_);
  data_.p_quality = submodel->MakeField<uint8_t>("quality");
  data_.subwriter = model->MakeCollection("Puppi", std::move(submodel));
}

void RNTupleUnpackerCollFloat::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  for (uint16_t i = 0; i < nwords; ++i) {
    readshared(words[i], *data_.p_pt, *data_.p_eta, *data_.p_phi);
    if (readpid(words[i], *data_.p_pdgid)) {
      readcharged(words[i], *data_.p_z0, *data_.p_dxy, *data_.p_quality);
      *data_.p_wpuppi = 1;
    } else {
      readneutral(words[i], *data_.p_wpuppi, *data_.p_quality);
      *data_.p_z0 = 0;
      *data_.p_dxy = 0;
    }
    if ((writer_ || !commit) && data_.subwriter)
      data_.subwriter->Fill();
  }
  if (writer_ && commit)
    writer_->Fill();
}