#ifndef p2_clients_TTreeUnpackerRaw96_h
#define p2_clients_TTreeUnpackerRaw96_h
#include "TTreeUnpackerBase.h"
#include <TBranch.h>

class TTreeUnpackerRaw96 : public TTreeUnpackerBase {
public:
  TTreeUnpackerRaw96(const Spec &spec, const std::string &objName) : TTreeUnpackerBase(spec), objName_(objName) {}
  ~TTreeUnpackerRaw96() override {}

  void addMyOutput(TTree *tree) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  std::string objName_;
  struct Data {
    uint64_t lo[255];
    uint32_t hi[255];
  } data_;
};

#endif