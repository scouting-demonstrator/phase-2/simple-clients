#ifndef p2_clients_TTreeUnpackerBase_h
#define p2_clients_TTreeUnpackerBase_h
#include <chrono>
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include "../UnpackerBase.h"
#include "../unpack.h"

class TTreeUnpackerBase : public UnpackerBase {
public:
  TTreeUnpackerBase(const Spec &spec)
      : UnpackerBase(spec),
        compressionAlgo_(parseCompression(spec.compressionAlgo())),
        compressionLevel_(spec.compressionLevel()),
        fout_(),
        file_(nullptr),
        tree_(nullptr) {
    initMT(spec.numThreads());
  }
  ~TTreeUnpackerBase() override {}

  static void initMT(unsigned int threads);

  static int parseCompression(const std::string &algo);

  void bookOutput(const std::string &out) override;

  void bookOutputBase(const std::string &out);
  virtual void addMyOutput(TTree *tree) = 0;

  unsigned long int closeOutput() override;

  std::string outputExtension() override { return "root"; }

protected:
  int compressionAlgo_, compressionLevel_;
  std::string fout_;
  TFile *file_;
  TTree *tree_;
  uint16_t run_, bx_, npuppi_;
  uint32_t orbit_;
  Bool_t good_;
};

#endif