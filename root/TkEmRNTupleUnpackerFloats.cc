#include "TkEmRNTupleUnpackerFloats.h"
#include "../unpack.h"

void TkEmRNTupleUnpackerFloats::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("goodTkEm");
  data_.p_nem = model->MakeField<uint8_t>("nTkEm");
  data_.p_pt = makeVFloatField(*model, "TkEm_pt", float16_);
  data_.p_eta = makeVFloatField(*model, "TkEm_eta", float16_);
  data_.p_phi = makeVFloatField(*model, "TkEm_phi", float16_);
  data_.p_quality = model->MakeField<std::vector<uint8_t>>("TkEm_quality");
  data_.p_isolation = makeVFloatField(*model, "TkEm_isolation", float16_);
}

void TkEmRNTupleUnpackerFloats::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  uint16_t negamma = (nwords * 2) / 3;
  assert(negamma >= 12);
  uint16_t nem = 12, nele = negamma - 12;
  *data_.p_nem = nem;
  data_.p_pt->resize(nem);
  data_.p_eta->resize(nem);
  data_.p_phi->resize(nem);
  data_.p_quality->resize(nem);
  data_.p_isolation->resize(nem);
  decode_tkem(nwords,
              words,
              nem,
              &data_.p_pt->front(),
              &data_.p_eta->front(),
              &data_.p_phi->front(),
              &data_.p_quality->front(),
              &data_.p_isolation->front());
  *dataEle_.p_nem = nele;
  dataEle_.p_pt->resize(nele);
  dataEle_.p_eta->resize(nele);
  dataEle_.p_phi->resize(nele);
  dataEle_.p_quality->resize(nele);
  dataEle_.p_isolation->resize(nele);
  dataEle_.p_charge->resize(nele);
  dataEle_.p_z0->resize(nele);
  decode_tkele(nwords,
               words,
               nele,
               &dataEle_.p_pt->front(),
               &dataEle_.p_eta->front(),
               &dataEle_.p_phi->front(),
               &dataEle_.p_quality->front(),
               &dataEle_.p_isolation->front(),
               &dataEle_.p_charge->front(),
               &dataEle_.p_z0->front());
  if (writer_ && commit)
    writer_->Fill();
}