#include "GMTTkMuRNTupleUnpackerFloats.h"
#include "../unpack.h"

void GMTTkMuRNTupleUnpackerFloats::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("goodTkMu");
  data_.p_nmu = model->MakeField<uint8_t>("nTkMu");
  data_.p_pt = makeVFloatField(*model, "TkMu_pt", float16_);
  data_.p_eta = makeVFloatField(*model, "TkMu_eta", float16_);
  data_.p_phi = makeVFloatField(*model, "TkMu_phi", float16_);
  data_.p_charge = model->MakeField<std::vector<int8_t>>("TkMu_charge");
  data_.p_z0 = makeVFloatField(*model, "TkMu_z0", float16_);
  data_.p_d0 = makeVFloatField(*model, "TkMu_d0", float16_);
  data_.p_beta = makeVFloatField(*model, "TkMu_beta", float16_);
  data_.p_quality = model->MakeField<std::vector<uint8_t>>("TkMu_quality");
  data_.p_isolation = model->MakeField<std::vector<uint8_t>>("TkMu_isolation");
}

void GMTTkMuRNTupleUnpackerFloats::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  uint16_t nmu = (nwords * 2) / 3;
  *data_.p_nmu = nmu;
  data_.p_pt->resize(nmu);
  data_.p_eta->resize(nmu);
  data_.p_phi->resize(nmu);
  data_.p_charge->resize(nmu);
  data_.p_z0->resize(nmu);
  data_.p_d0->resize(nmu);
  data_.p_beta->resize(nmu);
  data_.p_quality->resize(nmu);
  data_.p_isolation->resize(nmu);
  decode_gmt_tkmu(nwords,
                  words,
                  nmu,
                  &data_.p_pt->front(),
                  &data_.p_eta->front(),
                  &data_.p_phi->front(),
                  &data_.p_charge->front(),
                  &data_.p_z0->front(),
                  &data_.p_d0->front(),
                  &data_.p_quality->front(),
                  &data_.p_isolation->front(),
                  &data_.p_beta->front());
  if (writer_ && commit)
    writer_->Fill();
}