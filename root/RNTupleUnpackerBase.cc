#include "RNTupleUnpackerBase.h"
#include <filesystem>
#include <cassert>

void RNTupleUnpackerBase::initMT(unsigned int threads) {
  if (threads > 0) {
    ROOT::EnableImplicitMT(threads);
    printf("Enabled Implicit MT with %d threads\n", threads);
  }
}

int RNTupleUnpackerBase::parseCompression(const std::string &algo, unsigned int compressionLevel) {
  if (algo == "lzma")
    return ROOT::CompressionSettings(ROOT::kLZMA, compressionLevel);
  else if (algo == "zlib")
    return ROOT::CompressionSettings(ROOT::kZLIB, compressionLevel);
  else if (algo == "lz4")
    return ROOT::CompressionSettings(ROOT::kLZ4, compressionLevel);
  else if (algo == "zstd")
    return ROOT::CompressionSettings(ROOT::kZSTD, compressionLevel);
  else if (algo == "none") {
    return 0;
  } else
    throw std::runtime_error("Unsupported compression algo " + algo);
  return 0;
}

unsigned long int RNTupleUnpackerBase::closeOutput() {
  unsigned long int ret = 0;
  if (writer_) {
    writer_->CommitCluster();
    writer_.reset();
    ret = std::filesystem::file_size(fout_);
    fout_.clear();
  }
  return ret;
}

void RNTupleUnpackerBase::bookBase(const std::string &out, std::unique_ptr<ROOT::Experimental::RNTupleModel> model) {
  if (!out.empty()) {
    assert(fout_.empty() && !writer_);
    fout_ = out;
    if (out.length() <= 5 || out.substr(out.length() - 5) != ".root") {
      fout_ = out + ".root";
    }
    ROOT::Experimental::RNTupleWriteOptions options;
    options.SetCompression(compression_);
    writer_ = ROOT::Experimental::RNTupleWriter::Recreate(std::move(model), "Events", fout_.c_str(), options);
  }
}

std::shared_ptr<float> RNTupleUnpackerBase::makeFloatField(ROOT::Experimental::RNTupleModel &model,
                                                           std::string_view name,
                                                           bool float16) {
  if (float16) {
#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 32, 00)
    auto fieldPtr = std::make_unique<ROOT::Experimental::RField<float>>(name);
    fieldPtr->SetHalfPrecision();
    model.AddField(std::move(fieldPtr));
    return model.GetDefaultEntry().GetPtr<float>(name);
#else
    throw std::runtime_error("float16 format requires root >= 6.32 ");
#endif
  } else {
    return model.MakeField<float>(name);
  }
}
std::shared_ptr<std::vector<float>> RNTupleUnpackerBase::makeVFloatField(ROOT::Experimental::RNTupleModel &model,
                                                                         std::string_view name,
                                                                         bool float16) {
  if (float16) {
#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 32, 00)
    auto fieldPtr = std::make_unique<ROOT::Experimental::RField<std::vector<float>>>(name);
    auto leafFieldPtr = dynamic_cast<ROOT::Experimental::RField<float> *>(fieldPtr->GetSubFields()[0]);
    leafFieldPtr->SetHalfPrecision();
    model.AddField(std::move(fieldPtr));
    return model.GetDefaultEntry().GetPtr<std::vector<float>>(name);
#else
    throw std::runtime_error("float16 format requires root >= 6.32 ");
#endif
  } else {
    return model.MakeField<std::vector<float>>(name);
  }
}