#ifndef p2_clients_TkEmRNTupleUnpackerCollFloat_h
#define p2_clients_TkEmRNTupleUnpackerCollFloat_h
#include "RNTupleUnpackerBase.h"

#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 32, 00)
  #include <ROOT/RNTupleCollectionWriter.hxx>
#endif

class TkEmRNTupleUnpackerCollFloat : public RNTupleUnpackerBase {
public:
  TkEmRNTupleUnpackerCollFloat(const Spec &spec, bool float16 = false) : RNTupleUnpackerBase(spec), float16_(float16) {}
  ~TkEmRNTupleUnpackerCollFloat() override {}

  void addMyOutput(ROOT::Experimental::RNTupleModel *model) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  struct Data {
    std::shared_ptr<float> p_pt, p_eta, p_phi, p_isolation;
    std::shared_ptr<uint8_t> p_quality;
#if ROOT_VERSION_CODE < ROOT_VERSION(6, 32, 00)
    std::shared_ptr<ROOT::Experimental::RCollectionNTupleWriter> subwriter;
#else
    std::shared_ptr<ROOT::Experimental::RNTupleCollectionWriter> subwriter;
#endif
  } data_;
  struct DataEle : public Data {
    std::shared_ptr<int8_t> p_charge;
    std::shared_ptr<float> p_z0;
  } dataEle_;

  bool float16_;
};

#endif