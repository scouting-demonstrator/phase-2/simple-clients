#include "RootUnpackMaker.h"

#include "RNTupleUnpackerBase.h"
#include "RNTupleUnpackerFloats.h"
#include "RNTupleUnpackerCollFloat.h"
#include "RNTupleUnpackerInts.h"
#include "RNTupleUnpackerCollInt.h"
#include "RNTupleUnpackerRaw64.h"
#include "RNTupleUnpackerRaw96.h"
#include "GMTTkMuRNTupleUnpackerFloats.h"
#include "GMTTkMuRNTupleUnpackerCollFloat.h"
#include "TkEmRNTupleUnpackerFloats.h"
#include "TkEmRNTupleUnpackerCollFloat.h"
#include "RNTupleUnpackerComposite.h"
#include "TTreeUnpackerBase.h"
#include "TTreeUnpackerFloats.h"
#include "TTreeUnpackerInts.h"
#include "TTreeUnpackerRaw64.h"
#include "TTreeUnpackerRaw96.h"
#include "TkEmTTreeUnpackerFloats.h"
#include "GMTTkMuTTreeUnpackerFloats.h"
#include "GMTTkMuTTreeUnpackerInts.h"
#include "TTreeUnpackerComposite.h"
#include <ROOT/RLogger.hxx>
#include <ROOT/RNTuple.hxx>
#include <ROOT/RNTupleUtil.hxx>
#include <stdexcept>

std::unique_ptr<UnpackerBase> RootUnpackMaker::make(const UnpackerBase::Spec &spec) {
  static bool _once = false;
  if (!_once) {
    _once = true;
    ROOT::EnableThreadSafety();
    static auto verbosity =
        ROOT::Experimental::RLogScopedVerbosity(ROOT::Experimental::NTupleLog(), ROOT::Experimental::ELogLevel::kError);
  }

  std::unique_ptr<UnpackerBase> unpacker;
  switch (spec.objType) {
    case Spec::ObjType::Puppi:
      if (spec.fileKind == TFileKind::TTree) {
        if (spec.format == "float" || spec.format == "float24") {
          unpacker = std::make_unique<TTreeUnpackerFloats>(spec);
        } else if (spec.format == "int") {
          unpacker = std::make_unique<TTreeUnpackerInts>(spec);
        } else if (spec.format == "raw64") {
          unpacker = std::make_unique<TTreeUnpackerRaw64>(spec);
        } else {
          throw std::invalid_argument("Unsupported puppi ttree format " + spec.format);
        }
        break;
      } else if (spec.fileKind == TFileKind::RNTuple) {
        if (spec.format == "floats" || spec.format == "floats16") {
          unpacker = std::make_unique<RNTupleUnpackerFloats>(spec, spec.format == "floats16");
        } else if (spec.format == "coll_float" || spec.format == "coll_float16") {
          unpacker = std::make_unique<RNTupleUnpackerCollFloat>(spec, spec.format == "coll_float16");
        } else if (spec.format == "ints") {
          unpacker = std::make_unique<RNTupleUnpackerInts>(spec);
        } else if (spec.format == "coll_int") {
          unpacker = std::make_unique<RNTupleUnpackerCollInt>(spec);
        } else if (spec.format == "raw64") {
          unpacker = std::make_unique<RNTupleUnpackerRaw64>(spec);
        } else {
          throw std::invalid_argument("Unsupported puppi rntuple format " + spec.format);
        }
      } else {
        throw std::invalid_argument("Unsupported puppi file kind " + spec.fileKind);
      }
      break;
    case Spec::ObjType::TkEm:
      if (spec.fileKind == TFileKind::TTree) {
        if (spec.format == "float" || spec.format == "float24") {
          unpacker = std::make_unique<TkEmTTreeUnpackerFloats>(spec);
        } else if (spec.format == "raw96") {
          unpacker = std::make_unique<TTreeUnpackerRaw96>(spec, "TkEm");
        } else {
          throw std::invalid_argument("Unsupported tkem ttree format " + spec.format);
        }
      } else if (spec.fileKind == TFileKind::RNTuple) {
        if (spec.format == "floats" || spec.format == "floats16") {
          unpacker = std::make_unique<TkEmRNTupleUnpackerFloats>(spec, spec.format == "floats16");
        } else if (spec.format == "coll_float" || spec.format == "coll_float16") {
          unpacker = std::make_unique<TkEmRNTupleUnpackerCollFloat>(spec, spec.format == "coll_float16");
        } else if (spec.format == "raw96") {
          unpacker = std::make_unique<RNTupleUnpackerRaw96>(spec, "TkEm");
        } else {
          throw std::invalid_argument("Unsupported tkem rntuple format " + spec.format);
        }
      } else {
        throw std::invalid_argument("Unsupported tkem file kind " + spec.fileKind);
      }
      break;
    case Spec::ObjType::TkMu:
      if (spec.fileKind == TFileKind::TTree) {
        if (spec.format == "float" || spec.format == "float24") {
          unpacker = std::make_unique<GMTTkMuTTreeUnpackerFloats>(spec);
        } else if (spec.format == "int") {
          unpacker = std::make_unique<GMTTkMuTTreeUnpackerInts>(spec);
        } else if (spec.format == "raw96") {
          unpacker = std::make_unique<TTreeUnpackerRaw96>(spec, "TkMu");
        } else {
          throw std::invalid_argument("Unsupported tkmu ttree format " + spec.format);
        }
      } else if (spec.fileKind == TFileKind::RNTuple) {
        if (spec.format == "floats" || spec.format == "floats16") {
          unpacker = std::make_unique<GMTTkMuRNTupleUnpackerFloats>(spec, spec.format == "floats16");
        } else if (spec.format == "coll_float" || spec.format == "coll_float16") {
          unpacker = std::make_unique<GMTTkMuRNTupleUnpackerCollFloat>(spec, spec.format == "coll_float16");
        } else if (spec.format == "raw96") {
          unpacker = std::make_unique<RNTupleUnpackerRaw96>(spec, "TkMu");
        } else {
          throw std::invalid_argument("Unsupported tkmu rntuple format " + spec.format);
        }
      } else {
        throw std::invalid_argument("Unsupported tkmu file kind " + spec.fileKind);
      }
      break;
    case Spec::ObjType::Composite:
      if (spec.fileKind == TFileKind::TTree) {
        unpacker = std::make_unique<TTreeUnpackerComposite>(spec);
      } else if (spec.fileKind == TFileKind::RNTuple) {
        unpacker = std::make_unique<RNTupleUnpackerComposite>(spec);
      } else {
        throw std::invalid_argument("Unsupported composite file kind " + spec.fileKind);
      }
      break;
  }

  return unpacker;
}