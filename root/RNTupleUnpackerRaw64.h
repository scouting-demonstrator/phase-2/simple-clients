#ifndef p2_clients_RNTupleUnpackerRaw64_h
#define p2_clients_RNTupleUnpackerRaw64_h
#include "RNTupleUnpackerBase.h"
#include <ROOT/RVec.hxx>

class RNTupleUnpackerRaw64 : public RNTupleUnpackerBase {
public:
  RNTupleUnpackerRaw64(const Spec &spec, const std::string &objName = "Puppi")
      : RNTupleUnpackerBase(spec), objName_(objName) {}
  ~RNTupleUnpackerRaw64() override {}

  void addMyOutput(ROOT::Experimental::RNTupleModel *model) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  std::string objName_;
  std::shared_ptr<ROOT::RVec<uint64_t>> p_data;
};

#endif