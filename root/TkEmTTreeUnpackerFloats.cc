#include "TkEmTTreeUnpackerFloats.h"
#include <TTree.h>
#include "../unpack.h"

void TkEmTTreeUnpackerFloats::addMyOutput(TTree *tree) {
  tree_ = tree;
  std::string F = (floatType_ == "float24" ? "f" : "F");
  if (tree_) {
    tree_->Branch("nTkEm", &data_.nem, "nTkEm/s");
    tree_->Branch("goodTkEm", &good_, "goodTkEm/O");
    tree_->Branch("TkEm_pt", &data_.pt, ("TkEm_pt[nTkEm]/" + F).c_str());
    tree_->Branch("TkEm_eta", &data_.eta, ("TkEm_eta[nTkEm]/" + F).c_str());
    tree_->Branch("TkEm_phi", &data_.phi, ("TkEm_phi[nTkEm]/" + F).c_str());
    tree_->Branch("TkEm_quality", &data_.quality, "TkEm_quality[nTkEm]/b");
    tree_->Branch("TkEm_isolation", &data_.isolation, ("TkEm_beta[nTkEm]/" + F).c_str());
    // and electrons
    tree_->Branch("nTkEle", &dataEle_.nem, "nTkEle/s");
    tree_->Branch("goodTkEle", &good_, "goodTkEle/O");
    tree_->Branch("TkEle_pt", &dataEle_.pt, ("TkEle_pt[nTkEle]/" + F).c_str());
    tree_->Branch("TkEle_eta", &dataEle_.eta, ("TkEle_eta[nTkEle]/" + F).c_str());
    tree_->Branch("TkEle_phi", &dataEle_.phi, ("TkEle_phi[nTkEle]/" + F).c_str());
    tree_->Branch("TkEle_quality", &dataEle_.quality, "TkEle_quality[nTkEle]/b");
    tree_->Branch("TkEle_isolation", &dataEle_.isolation, ("TkEle_beta[nTkEle]/" + F).c_str());
    tree_->Branch("TkEle_charge", &dataEle_.charge, "TkEle_charge[nTkEle]/B");
    tree_->Branch("TkEle_z0", &dataEle_.z0, ("TkEle_z0[nTkEle]/" + F).c_str());
  }
}

void TkEmTTreeUnpackerFloats::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  run_ = run;
  orbit_ = orbit;
  bx_ = bx;
  good_ = good;
  npuppi_ = nwords;
  decode_tkem(nwords, words, data_.nem, data_.pt, data_.eta, data_.phi, data_.quality, data_.isolation);
  decode_tkele(nwords,
               words,
               dataEle_.nem,
               dataEle_.pt,
               dataEle_.eta,
               dataEle_.phi,
               dataEle_.quality,
               dataEle_.isolation,
               dataEle_.charge,
               dataEle_.z0);
  if (tree_ && commit)
    tree_->Fill();
}