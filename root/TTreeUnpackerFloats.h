#ifndef p2_clients_TTreeUnpackerFloats_h
#define p2_clients_TTreeUnpackerFloats_h
#include "TTreeUnpackerBase.h"
#include <stdlib.h>

class TTreeUnpackerFloats : public TTreeUnpackerBase {
public:
  TTreeUnpackerFloats(const Spec &spec) : TTreeUnpackerBase(spec), floatType_(spec.format) {}
  ~TTreeUnpackerFloats() override {}

  void addMyOutput(TTree *tree) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  std::string floatType_;

  struct Data {
    //puppi candidate info:
    float pt[255];
    float eta[255], phi[255];
    short int pdgid[255];
    uint8_t quality[255];
    //charged only:
    float z0[255];
    float dxy[255];
    //neutral only:
    float wpuppi[255];
  };

  Data data_;
};

#endif