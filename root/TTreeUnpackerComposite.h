#ifndef p2_clients_TTreeUnpackerComposite_h
#define p2_clients_TTreeUnpackerComposite_h
#include "TTreeUnpackerBase.h"
#include <stdlib.h>
#include <vector>
#include <memory>

class TTreeUnpackerComposite : public TTreeUnpackerBase {
public:
  TTreeUnpackerComposite(const Spec &spec);
  ~TTreeUnpackerComposite() override {}

  void addMyOutput(TTree *tree) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;
};

#endif