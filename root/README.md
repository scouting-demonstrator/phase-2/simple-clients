# ROOT unpackers for Phase-2 L1 Data Scouting formats

All the tools here read data in **Native64** format and unpack it to [ROOT](https://root.cern.ch) format, either as [TTree](https://root.cern.ch/doc/master/classTTree.html) or [RNTuple](https://root.cern/doc/v626/structROOT_1_1Experimental_1_1RNTuple.html).

For testing a small data file Puppi.dump in **Native64** format is available under `data`.

The unpackers can also read round-robin from N files simultaneously, e.g to reassemble the raw outputs from N time-multiplexed nodes.

The unpackers can read RAW files in *CMSSW* format if the  `--cmssw` option is passed on the command line.

## Unpacker formats

The unpacker can be run as `p2scout-unpacker <objType> <fileType> <format> inputs.dump [output.root]`.
 * The object type can be `puppi` for Puppi candidates, and `tkmu` for GMT TkMuons.
 * The file type can be `ttree` or `rntuple`
 * The each object type and file type supports different fromats, as documented below

A `composite` object type can also be used, to unpack simultaneously different object types into a single `ttree`, as documented below.

#### Puppi object TTree formats (`p2scout-unpacker puppi ttree`)
The unpacker produces one list of Puppi objects, in different possible formats.
 * `float`: data members of the puppi candidates are converted to floating point values in natural units (GeV, cm), and particle IDs are converted to pdgId codes and saved as short integers. Data members available only for charged or only for neutral particles are filled with zeros for the particles of the wrong type.
 * `float24`: as above, but with truncated mantissa (ROOT calls it `Float16_t`, but it's actually 24 bits)
 * `int`: data members of the puppi candidates are unpacked to signed or unsigned integers large enough to fit the range (mainly `int16_t` or `uint16_t`) with the units natively by the L1T (e.g. 1 unit of p<sub>T</sub> is 0.25 GeV, 1 unit of &eta; or &phi; is &pi;/720), and particle IDs are saved as-is (range 0-7). Data members available only for charged or only for neutral particles are filled with zeros for the particles of the wrong type.
 * `raw64`: each puppi candidate is saved as a packed `uint64_t` word

#### Puppi object RNTuple formats (`p2scout-unpacker puppi rntuple`)
The unpacker have different layouts and formats:
 * `floats`, `floats16`: data members are unpacked to floats (same as the `float` format of the TTree unpacker above), and each variable is saved independently as `std::vector<float>` (or of short integers)
   * In the `floats16` versions, the on-disk format is the [IEEE float16](https://en.wikipedia.org/wiki/Half-precision_floating-point_format)
 * `coll_float`, `coll_float16`: Puppi objects are saved as a RNTuple collection, with individual variables saved as floats (or float16 on disk).
 * `ints`, `coll_int`: data members are unpacked to integers, as per the `int` format of the TTree unpacker, and saved as std::vector's (`ints`) or using a RNTuple collection (`coll_int`).
 * `raw64`:  each puppi candidate is saved as a packed `uint64_t` word

#### TkEm object TTree formats (`p2scout-unpacker tkem ttree`)
The unpacker produces one list of TkEm objects and one of TkEle objects, in different possible formats.
 * `float`: data members of the TkEm and TkEle candidates are converted to floating point values in natural units (GeV, cm). The quality value is kept as uint8.
 * `float24`: as above, but with truncated mantissa (ROOT calls it `Float16_t`, but it's actually 24 bits)
 * `raw96`:  each TkEm and TkEle candidate is saved as a packed `uint64_t` low word `lo` with bits 63-0, and a packed `uint32_t` high word `hi` (bits 95-64). Note that in this format the raw unpacker photons and electrons are saved in the same list, with the first 12 elements being photons (or zeroes) and the remaining being electrons.

#### TkMu object TTree formats (`p2scout-unpacker tkmu ttree`)
The unpacker produces one list of TkMu objects, in different possible formats.
 * `float`: data members of the TkMu candidates are converted to floating point values in natural units (GeV, cm), and the charge is converted to an int8 (values &plusmn;1). The isolation bitmask and quality values are kept as uint8.
 * `float24`: as above, but with truncated mantissa (ROOT calls it `Float16_t`, but it's actually 24 bits)
 * `int`: data members of the TkMu candidates are unpacked to signed or unsigned integers large enough to fit the range (mainly `int16_t` or `uint16_t`) with the units natively by the L1T (e.g. 1 unit of p<sub>T</sub> is 31.25 MeV, 1 unit of &eta; or &phi; is &pi;/2<sup>12</sup>). The charge is still converted to an int8 (values &plusmn;1).
 * `raw96`:  each TkMu candidate is saved as a packed `uint64_t` low word `lo` with bits 63-0, and a packed `uint32_t` high word `hi` (bits 95-64)

#### TkEm, TkMu object RNTuple formats (`p2scout-unpacker tkem rntuple`, `p2scout-unpacker tkmu rntuple`)

The unpacker supports these formats:
 * `floats`, `floats16`: data members are unpacked to floats (same as the `float` format of the TTree unpacker above), and each variable is saved independently as `std::vector<float>` (or of short integers). 
 * `coll_float`, `coll_float16`: objects are saved as a RNTuple collection, with individual variables saved as floats.
In the `float16` versions, the on-disk format is the [IEEE float16](https://en.wikipedia.org/wiki/Half-precision_floating-point_format)
 * `raw96`:  each candidate is saved as a packed `uint64_t` low word `lo` with bits 63-0, and a packed `uint32_t` high word `hi` (bits 95-64)


### Composite unpacker (`p2scout-unpacker composite ttree`)

It is also possible to unpack simultaneously different object types into a single `ttree` or `rntuple`, by running `p2scout-unpacker composite (ttree|rntuple) <format> inputs.dump [output.root]`.
 * the format should be a list `<objType1>-<format1>[-<timeslices1>][,<objType2>-<format2>-[<timeslices2>][-...]]`, where each individual object type and format are the ones supported by the individual unpackers
 * the order of the input files should match the format specified, e.g. if doing `puppi-float-6,tkmu-float-18` there should be 6+18=24 input files, the first 6 of which should be the 6 timeslices of the puppi inputs, and the remaining 18 should be the 18 timeslices of the tk muons.

## Running the code

See the instructions in the parent directory on how to get an environment to compile the code.

Different tests and benchmarks are implemented, and can be run with `make <test>`:
 * `run_tests`: just tests all the different unpacker formats
 * `run_test_speed`: test speed and output size for puppi uncompressed formats. 
 * `run_test_comp`: test speed and output size for puppi compressed formats..
 * `run_test_multi`: runs the unpacker reading round-robin from 6 copies of the puppi input file
 * `run_test_tkmu`:  test speed and output size for tkmu formats (uncompressed and compressed)
For most tests, if you have a larger input data sample, you can point the code to it with `DATAFILE=/path/to/file make <test>`

The full functionality of the core requires [ROOT 6.32.02](https://root.cern/releases/release-63202/) or later. It compiles also in ROOT 6.30 but support for float16 in RNTuple will not be available.


