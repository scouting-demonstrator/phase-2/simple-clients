#include "RNTupleUnpackerRaw96.h"
#include <chrono>
#include "../unpack.h"

void RNTupleUnpackerRaw96::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("good" + objName_);
  p_data_lo = model->MakeField<ROOT::RVec<uint64_t>>(objName_ + "_lo");
  p_data_hi = model->MakeField<ROOT::RVec<uint32_t>>(objName_ + "_hi");
}

void RNTupleUnpackerRaw96::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  uint16_t n96 = (nwords * 2) / 3;
  p_data_hi->resize(n96);
  p_data_lo->resize(n96);
  unpack_w96_gtencoding(nwords, words, n96, p_data_lo->data(), p_data_hi->data());
  if (writer_ && commit)
    writer_->Fill();
}