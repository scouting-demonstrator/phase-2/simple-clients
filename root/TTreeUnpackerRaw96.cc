#include "TTreeUnpackerRaw96.h"
#include <TTree.h>
#include "../unpack.h"

void TTreeUnpackerRaw96::addMyOutput(TTree *tree) {
  tree_ = tree;
  if (tree_) {
    tree_->Branch(("n" + objName_).c_str(), &npuppi_, ("n" + objName_ + "/s").c_str());
    tree_->Branch(("good" + objName_).c_str(), &good_, ("good" + objName_ + "/O").c_str());
    tree_->Branch(objName_.c_str(), data_.lo, (objName_ + "_lo[n" + objName_ + "]/l").c_str());
    tree_->Branch(objName_.c_str(), data_.hi, (objName_ + "_hi[n" + objName_ + "]/i").c_str());
  }
}

void TTreeUnpackerRaw96::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  run_ = run;
  orbit_ = orbit;
  bx_ = bx;
  good_ = good;
  unpack_w96_gtencoding(nwords, words, npuppi_, data_.lo, data_.hi);
  if (tree_ && commit)
    tree_->Fill();
}