#include "TTreeUnpackerRaw64.h"
#include <TTree.h>
#include "../unpack.h"

void TTreeUnpackerRaw64::addMyOutput(TTree *tree) {
  tree_ = tree;
  uint64_t dummy[1];
  if (tree_) {
    tree_->Branch(("n" + objName_).c_str(), &npuppi_, ("n" + objName_ + "/s").c_str());
    tree_->Branch(("good" + objName_).c_str(), &good_, ("good" + objName_ + "/O").c_str());
    branch_ = tree_->Branch(objName_.c_str(), dummy, (objName_ + "[n" + objName_ + "]/l").c_str());
  }
}

void TTreeUnpackerRaw64::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  run_ = run;
  orbit_ = orbit;
  bx_ = bx;
  good_ = good;
  npuppi_ = nwords;
  if (tree_) {
    branch_->SetAddress((void *)words);  // bad TTree API, should be const void *
    if (commit)
      tree_->Fill();
  }
}