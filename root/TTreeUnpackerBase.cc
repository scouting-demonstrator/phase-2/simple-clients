#include "TTreeUnpackerBase.h"
#include <filesystem>

void TTreeUnpackerBase::initMT(unsigned int threads) {
  if (threads != 0) {
    ROOT::EnableImplicitMT(threads);
    printf("Enabled Implicit MT with %d threads\n", threads);
  }
}

int TTreeUnpackerBase::parseCompression(const std::string &algo) {
  if (algo == "lzma")
    return ROOT::RCompressionSetting::EAlgorithm::kLZMA;
  else if (algo == "zlib")
    return ROOT::RCompressionSetting::EAlgorithm::kZLIB;
  else if (algo == "lz4")
    return ROOT::RCompressionSetting::EAlgorithm::kLZ4;
  else if (algo == "zstd")
    return ROOT::RCompressionSetting::EAlgorithm::kZSTD;
  else if (algo == "none") {
    return ROOT::RCompressionSetting::EAlgorithm::kZLIB;
  } else
    throw std::runtime_error("Unsupported compression algo " + algo);
  return 0;
}

void TTreeUnpackerBase::bookOutput(const std::string &out) {
  bookOutputBase(out);
  addMyOutput(tree_);
}

void TTreeUnpackerBase::bookOutputBase(const std::string &out) {
  if (!out.empty()) {
    assert(file_ == nullptr && tree_ == nullptr);
    fout_ = out;
    if (out.length() <= 5 || out.substr(out.length() - 5) != ".root") {
      fout_ = out + ".root";
    }
    file_ = TFile::Open(fout_.c_str(), "RECREATE", "", compressionLevel_);
    if (file_ == nullptr || !file_) {
      throw std::runtime_error("Error opening " + fout_ + " for output");
    }
    if (compressionLevel_)
      file_->SetCompressionAlgorithm(compressionAlgo_);
    tree_ = new TTree("Events", "Events");
    tree_->Branch("run", &run_, "run/s");
    tree_->Branch("orbit", &orbit_, "orbit/i");
    tree_->Branch("bx", &bx_, "bx/s");
  }
}

unsigned long int TTreeUnpackerBase::closeOutput() {
  unsigned long int ret = 0;
  if (tree_) {
    tree_->Write();
    file_->Close();
    tree_ = nullptr;
    file_ = nullptr;
    ret = std::filesystem::file_size(fout_);
    fout_.clear();
  }
  return ret;
}
