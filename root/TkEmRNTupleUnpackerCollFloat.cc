#include "TkEmRNTupleUnpackerCollFloat.h"
#include "../unpack.h"

void TkEmRNTupleUnpackerCollFloat::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("goodTkEm");
  auto submodel = ROOT::Experimental::RNTupleModel::Create();
  data_.p_pt = makeFloatField(*submodel, "pt", float16_);
  data_.p_eta = makeFloatField(*submodel, "eta", float16_);
  data_.p_phi = makeFloatField(*submodel, "phi", float16_);
  data_.p_quality = submodel->MakeField<uint8_t>("quality");
  data_.p_isolation = makeFloatField(*submodel, "isolation", float16_);
  data_.subwriter = model->MakeCollection("TkEm", std::move(submodel));
  auto submodelEle = ROOT::Experimental::RNTupleModel::Create();
  dataEle_.p_pt = makeFloatField(*submodelEle, "pt", float16_);
  dataEle_.p_eta = makeFloatField(*submodelEle, "eta", float16_);
  dataEle_.p_phi = makeFloatField(*submodelEle, "phi", float16_);
  dataEle_.p_quality = submodelEle->MakeField<uint8_t>("quality");
  dataEle_.p_isolation = makeFloatField(*submodelEle, "isolation", float16_);
  dataEle_.p_charge = submodelEle->MakeField<int8_t>("charge");
  dataEle_.p_z0 = makeFloatField(*submodelEle, "z0", float16_);
  dataEle_.subwriter = model->MakeCollection("TkEle", std::move(submodelEle));
}

void TkEmRNTupleUnpackerCollFloat::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  int negamma = (nwords * 2) / 3;
  assert(negamma >= 12);
  int nem = 12, nele = negamma - 12;
  const uint32_t *ptr32 = reinterpret_cast<const uint32_t *>(words);
  for (int i = 0; i < nem; ++i, ptr32 += 3) {
    uint64_t wlo;
    //uint32_t whi; // currently unused
    if ((i & 1) == 0) {
      wlo = *reinterpret_cast<const uint64_t *>(ptr32);
      //whi = *(ptr32 + 2);
    } else {
      wlo = *reinterpret_cast<const uint64_t *>(ptr32 + 1);
      //whi = *ptr32;
    }
    *data_.p_pt = extractBitsFromW<1, 16>(wlo) * 0.03125f;
    *data_.p_phi = extractSignedBitsFromW<17, 13>(wlo) * float(M_PI / (1 << 12));
    *data_.p_eta = extractSignedBitsFromW<30, 14>(wlo) * float(M_PI / (1 << 12));
    *data_.p_quality = extractBitsFromW<44, 4>(wlo);
    *data_.p_isolation = extractBitsFromW<48, 11>(wlo) * 0.25f;
    if ((writer_ || !commit) && data_.subwriter)
      data_.subwriter->Fill();
  }
  for (int i = 0; i < nele; ++i, ptr32 += 3) {
    uint64_t wlo;
    uint32_t whi;  // currently unused
    if ((i & 1) == 0) {
      wlo = *reinterpret_cast<const uint64_t *>(ptr32);
      whi = *(ptr32 + 2);
    } else {
      wlo = *reinterpret_cast<const uint64_t *>(ptr32 + 1);
      whi = *ptr32;
    }
    *dataEle_.p_pt = extractBitsFromW<1, 16>(wlo) * 0.03125f;
    *dataEle_.p_phi = extractSignedBitsFromW<17, 13>(wlo) * float(M_PI / (1 << 12));
    *dataEle_.p_eta = extractSignedBitsFromW<30, 14>(wlo) * float(M_PI / (1 << 12));
    *dataEle_.p_quality = extractBitsFromW<44, 4>(wlo);
    *dataEle_.p_isolation = extractBitsFromW<48, 11>(wlo) * 0.25f;
    *dataEle_.p_charge = (wlo & (1llu << 59)) ? -1 : +1;
    uint16_t z0raw = extractBitsFromW<60, 4>(wlo) | (extractBitsFromW<0, 6>(whi) << 6);
    *dataEle_.p_z0 = extractSignedBitsFromW<0, 10>(z0raw) * 0.05f;
    if ((writer_ || !commit) && dataEle_.subwriter)
      dataEle_.subwriter->Fill();
  }
  if (writer_ && commit)
    writer_->Fill();
}