#include "TTreeUnpackerComposite.h"
#include <cassert>
#include <typeinfo>

TTreeUnpackerComposite::TTreeUnpackerComposite(const Spec &spec) : TTreeUnpackerBase(spec) {
  for (auto &src : sources_) {
    if (!dynamic_cast<TTreeUnpackerBase *>(src.unpacker.get())) {
      throw std::logic_error("TTreeUnpackerComposite has a child which is not a TreeUnpackerBase but rather a " +
                             std::string(typeid(*src.unpacker).name()));
    }
  }
}

void TTreeUnpackerComposite::addMyOutput(TTree *tree) {
  tree_ = tree;
  if (tree_) {
    tree_->Branch("good", &good_, "good/O");
    for (auto &src : sources_) {
      (dynamic_cast<TTreeUnpackerBase &>(*src.unpacker)).addMyOutput(tree_);
    }
  }
}

void TTreeUnpackerComposite::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  assert(nwords == 0 && words == nullptr && commit);
  run_ = run;
  orbit_ = orbit;
  bx_ = bx;
  good_ = good;
  if (tree_)
    tree_->Fill();
}
