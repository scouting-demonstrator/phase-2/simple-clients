#ifndef p2_clients_RootUnpackMaker_h
#define p2_clients_RootUnpackMaker_h
#include "../UnpackerBase.h"
#include <memory>
#include <string_view>

namespace RootUnpackMaker {
  struct TFileKind {
    static constexpr std::string_view TTree = "ttree";
    static constexpr std::string_view RNTuple = "rntuple";
  };
  typedef UnpackerBase::Spec Spec;
  std::unique_ptr<UnpackerBase> make(const Spec &spec);
  inline bool accept(const std::string &fileKind) {
    return fileKind == TFileKind::TTree || fileKind == TFileKind::RNTuple;
  }
};  // namespace RootUnpackMaker

#endif