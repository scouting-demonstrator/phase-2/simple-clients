#ifndef p2_clients_RNTupleUnpackerComposite_h
#define p2_clients_RNTupleUnpackerComposite_h
#include "RNTupleUnpackerBase.h"
#include <stdlib.h>
#include <vector>
#include <memory>

class RNTupleUnpackerComposite : public RNTupleUnpackerBase {
public:
  RNTupleUnpackerComposite(const Spec &spec);
  ~RNTupleUnpackerComposite() override {}

  virtual void addMyOutput(ROOT::Experimental::RNTupleModel *model);

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;
};

#endif