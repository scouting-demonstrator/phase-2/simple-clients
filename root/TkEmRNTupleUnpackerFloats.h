#ifndef p2_clients_TkEmRNTupleUnpackerFloats_h
#define p2_clients_TkEmRNTupleUnpackerFloats_h
#include "RNTupleUnpackerBase.h"

class TkEmRNTupleUnpackerFloats : public RNTupleUnpackerBase {
public:
  TkEmRNTupleUnpackerFloats(const Spec &spec, bool float16 = false) : RNTupleUnpackerBase(spec), float16_(float16) {}
  ~TkEmRNTupleUnpackerFloats() override {}

  void addMyOutput(ROOT::Experimental::RNTupleModel *model) override final;

  void fillEvent(uint16_t run,
                 uint32_t orbit,
                 uint16_t bx,
                 bool good,
                 uint16_t nwords,
                 const uint64_t *words,
                 bool commit) override final;

protected:
  struct Data {
    std::shared_ptr<uint8_t> p_nem;
    std::shared_ptr<std::vector<float>> p_pt, p_eta, p_phi, p_isolation;
    std::shared_ptr<std::vector<uint8_t>> p_quality;
  } data_;
  struct DataEle : public Data {
    std::shared_ptr<std::vector<int8_t>> p_charge;
    std::shared_ptr<std::vector<float>> p_z0;
  } dataEle_;

  bool float16_;
};

#endif