#include "RNTupleUnpackerInts.h"
#include "../unpack.h"

void RNTupleUnpackerInts::addMyOutput(ROOT::Experimental::RNTupleModel *model) {
  dataBase_.p_good = model->MakeField<bool>("goodPuppi");
  data_.p_npuppi = model->MakeField<uint8_t>("nPuppi");
  data_.p_pt = model->MakeField<std::vector<uint16_t>>("Puppi_pt");
  data_.p_eta = model->MakeField<std::vector<int16_t>>("Puppi_eta");
  data_.p_phi = model->MakeField<std::vector<int16_t>>("Puppi_phi");
  data_.p_pid = model->MakeField<std::vector<uint8_t>>("Puppi_pid");
  data_.p_z0 = model->MakeField<std::vector<int16_t>>("Puppi_z0");
  data_.p_dxy = model->MakeField<std::vector<int8_t>>("Puppi_dxy");
  data_.p_quality = model->MakeField<std::vector<uint8_t>>("Puppi_quality");
  data_.p_wpuppi = model->MakeField<std::vector<uint16_t>>("Puppi_wpuppi");
}

void RNTupleUnpackerInts::fillEvent(
    uint16_t run, uint32_t orbit, uint16_t bx, bool good, uint16_t nwords, const uint64_t *words, bool commit) {
  if (dataBase_.p_run) {
    *dataBase_.p_run = run;
    *dataBase_.p_orbit = orbit;
    *dataBase_.p_bx = bx;
  }
  *dataBase_.p_good = good;
  *data_.p_npuppi = nwords;
  data_.p_pt->resize(nwords);
  data_.p_eta->resize(nwords);
  data_.p_phi->resize(nwords);
  data_.p_pid->resize(nwords);
  data_.p_z0->resize(nwords);
  data_.p_dxy->resize(nwords);
  data_.p_wpuppi->resize(nwords);
  data_.p_quality->resize(nwords);
  unpack_puppi_ints(nwords,
                    words,
                    data_.p_pt->data(),
                    data_.p_eta->data(),
                    data_.p_phi->data(),
                    data_.p_pid->data(),
                    data_.p_quality->data(),
                    data_.p_z0->data(),
                    data_.p_dxy->data(),
                    data_.p_wpuppi->data());
  if (writer_ && commit)
    writer_->Fill();
}