#include "UnpackerBase.h"
#include "UnpackMaker.h"
#include "unpack.h"
#include <chrono>
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <cassert>

UnpackerBase::Spec::Spec(const std::string &obj,   // puppi or tkmu
                         const std::string &kind,  // ttree or rntuple
                         const std::string &unpackFormat,
                         const Options &options)
    : fileKind(kind), format(unpackFormat), options(options) {
  if (obj == "puppi") {
    objType = ObjType::Puppi;
  } else if (obj == "tkem") {
    objType = ObjType::TkEm;
  } else if (obj == "tkmu") {
    objType = ObjType::TkMu;
  } else if (obj == "composite") {
    objType = ObjType::Composite;
  } else {
    throw std::invalid_argument("Unsupported object type '" + obj + "', must be 'puppi', 'tkmu' or 'composite'");
  }
}

UnpackerBase::UnpackerBase(const Spec &spec)
    : cmsswHeaders_(spec.boolOption("cmsswHeaders")), composite_(spec.objType == Spec::ObjType::Composite) {
  if (composite_) {
    std::string::size_type start = 0, end = std::string::npos;
    uint16_t offset = 0;
    do {
      end = spec.format.find(',', start);
      auto piece = spec.format.substr(start, end);
      auto i0 = piece.find('-'), i1 = piece.find('-', i0 + 1);
      if (i0 == std::string::npos)
        throw std::invalid_argument("Format '" + spec.format +
                                    "' doesn't match obj-fmt[-tslices](,obj-fmt[-tslices])*, at '" + piece + "'");
      std::string obj = piece.substr(0, i0), fmt = piece.substr(i0 + 1, i1 - i0 - 1);
      uint16_t timeslices = (i1 != std::string::npos) ? std::stoi(piece.substr(i1 + 1)) : 1;
      sources_.emplace_back(timeslices, offset, UnpackMaker::make(obj, spec.fileKind, fmt, spec.options));
      assert(sources_.back().unpacker);
      offset += timeslices;
      start = end + 1;
    } while (end != std::string::npos);
  }
}

UnpackerBase::~UnpackerBase() {}

UnpackerBase::Report UnpackerBase::unpackFiles(const std::vector<std::string> &ins, const std::string &out) {
  assert(!ins.empty());
  unsigned int ninputs = ins.size();
  if (composite_ && sources_.back().endOffset != ninputs)
    throw std::invalid_argument("Mismatched number of inputs, found " + std::to_string(ins.size()) + ", expected " +
                                std::to_string(sources_.back().endOffset));
  bookOutput(out);
  Report ret(0);
  uint32_t cmsswRun = 0, cmsswLumi = 0, cmsswNOrbits = 0, cmsswOrbit = 0;

  auto tstart = std::chrono::steady_clock::now();
  std::vector<std::fstream> fins;
  for (auto &in : ins) {
    auto &fin = fins.emplace_back(in, std::ios_base::in | std::ios_base::binary);
    if (!fin.good()) {
      throw std::runtime_error("Error opening " + in + " for input");
    }
    if (cmsswHeaders_) {
      char cmsswFileHeader[32];
      fin.read(cmsswFileHeader, 32);
      if (!fin.good() || (std::string(cmsswFileHeader, cmsswFileHeader + 8) != "RAW_0002") ||
          (*reinterpret_cast<uint16_t *>(&cmsswFileHeader[8]) != 32) ||
          (*reinterpret_cast<uint16_t *>(&cmsswFileHeader[10]) != 20)) {
        throw std::runtime_error("Error reading CMSSW file header from " + in);
      }
      if (cmsswRun == 0) {
        cmsswRun = *reinterpret_cast<uint32_t *>(&cmsswFileHeader[16]);
        cmsswLumi = *reinterpret_cast<uint32_t *>(&cmsswFileHeader[20]);
        cmsswNOrbits = *reinterpret_cast<uint32_t *>(&cmsswFileHeader[12]);
        if (cmsswRun == 0 || cmsswLumi == 0) {
          throw std::runtime_error("Error reading CMSSW file header from " + in + " bad run " +
                                   std::to_string(cmsswRun) + " or lumi " + std::to_string(cmsswLumi));
        }
        printf("CMSSW run %u, lumi %u, with %u orbits.\n", cmsswRun, cmsswLumi, cmsswNOrbits);
      } else if ((cmsswRun != *reinterpret_cast<uint32_t *>(&cmsswFileHeader[16])) ||
                 (cmsswLumi != *reinterpret_cast<uint32_t *>(&cmsswFileHeader[20])) ||
                 (cmsswNOrbits != *reinterpret_cast<uint32_t *>(&cmsswFileHeader[12]))) {
        throw std::runtime_error(
            "Mismatch between CMSSW file headers from " + in + " for " + " run " + std::to_string(cmsswRun) + " vs " +
            std::to_string(*reinterpret_cast<uint32_t *>(&cmsswFileHeader[16])) + " lumi " + std::to_string(cmsswLumi) +
            " vs " + std::to_string(*reinterpret_cast<uint32_t *>(&cmsswFileHeader[20])) + " norbits " +
            std::to_string(cmsswNOrbits) + " vs " +
            std::to_string(*reinterpret_cast<uint32_t *>(&cmsswFileHeader[12])));
      }
    }
    ret.bytes_in += std::filesystem::file_size(in);
  }

  // for composite
  unsigned int nsrcs = sources_.size();
  std::vector<unsigned int> ifiles;
  for (Source &src : sources_)
    ifiles.push_back(src.endOffset - 1);

  uint16_t run = 0;
  uint32_t orbit;
  uint16_t bx, hbx;
  bool good;
  uint16_t nwords;
  uint64_t header, payload[1 << 12];

  if (!cmsswHeaders_) {
    if (!composite_) {
      // simple loop on events
      for (int ifile = 0, nfiles = fins.size(); fins[ifile].good(); ifile = (ifile == nfiles - 1 ? 0 : ifile + 1)) {
        std::fstream &fin = fins[ifile];
        do {
          fin.read(reinterpret_cast<char *>(&header), sizeof(uint64_t));
        } while (header == 0 && fin.good());
        if (!fin.good())
          continue;
        parseHeader(header, run, bx, orbit, good, nwords);
        if (nwords)
          fin.read(reinterpret_cast<char *>(payload), nwords * sizeof(uint64_t));
        fillEvent(run, orbit, bx, good, nwords, payload);
        ret.entries++;
      }
    } else {
      // need to loop on sources, for each bx
      uint32_t oldorbit = 0;
      bx = 1;
      for (;;) {
        bool anygood = false, allgood = true;
        for (unsigned int isrc = 0; isrc < nsrcs; ++isrc) {
          Source &src = sources_[isrc];
          ifiles[isrc]++;
          if (ifiles[isrc] == src.endOffset)
            ifiles[isrc] = src.startOffset;
          std::fstream &fin = fins[ifiles[isrc]];
          header = 0;
          do {
            fin.read(reinterpret_cast<char *>(&header), sizeof(uint64_t));
          } while (header == 0 && fin.good());
          if (!fin.good()) {
            allgood = false;
            continue;
          }
          anygood = true;
          parseHeader(header, run, hbx, orbit, good, nwords);
          if (orbit != oldorbit) {
            bx = 1;
            oldorbit = orbit;
          } else {
            bx++;
          }
          if (hbx != bx)
            throw std::runtime_error("BX mismatch in source " + std::to_string(isrc) + ", file " +
                                     std::to_string(ifiles[isrc]) + ", bx " + std::to_string(hbx) +
                                     " while expecting " + std::to_string(bx));
          if (nwords)
            fin.read(reinterpret_cast<char *>(payload), nwords * sizeof(uint64_t));
          src.unpacker->fillEvent(run, orbit, bx, good, nwords, payload, false);
        }
        if (!anygood)
          break;
        if (!allgood)
          throw std::runtime_error("Mismatch between different input files");
        fillEvent(run, orbit, bx, allgood, 0, nullptr, true);
        ret.entries++;
      }
    }  // no CMSSW headers, composite
  } else {
    unsigned int nfiles = fins.size();
    std::vector<unsigned int> sizes(nfiles, 0);
    bool anyNull = false;
    for (unsigned int iorbit = 0; iorbit < cmsswNOrbits; ++iorbit) {
      // first read all orbit headers
      for (unsigned int ifile = 0; ifile < nfiles && fins[ifile].good(); ++ifile) {
        std::fstream &fin = fins[ifile];
        uint32_t orbitHeader[6];
        fin.read(reinterpret_cast<char *>(orbitHeader), 24);
        if (!fin.good() || (orbitHeader[0] != 6) || (orbitHeader[1] != cmsswRun) || (orbitHeader[2] != cmsswLumi) ||
            (orbitHeader[4] % 8 != 0) || ((ifile != 0) && (orbitHeader[3] != cmsswOrbit))) {
          printf("file %u, orbit %u, header: %u %u %u %u %u %u\n",
                 ifile,
                 iorbit,
                 orbitHeader[0],
                 orbitHeader[1],
                 orbitHeader[2],
                 orbitHeader[3],
                 orbitHeader[4],
                 orbitHeader[5]);
          throw std::runtime_error("Failed reading orbit " + std::to_string(iorbit) + " from file " + ins[ifile]);
        }
        cmsswOrbit = orbitHeader[3];
        sizes[ifile] = orbitHeader[4] >> 3;
        if (sizes[ifile] == 0)
          anyNull = true;
      }
      if (anyNull) {
        printf("Skipping orbit %u since it's null in at least one of the sources:\n", cmsswOrbit);
        for (unsigned int ifile = 0; ifile < nfiles && fins[ifile].good(); ++ifile) {
          if (sizes[ifile] == 0) {
            printf(" - source %u: %s\n", ifile, ins[ifile].c_str());
          } else {
            fins[ifile].seekg(sizes[ifile], std::ios_base::cur);
          }
        }
        continue;
      }
      //printf("Will read orbit %u (%u/%u)\n", cmsswOrbit, iorbit, cmsswNOrbits);
      if (!composite_) {
        // then loop to read events
        for (unsigned int ifile = 0; fins[ifile].good() && sizes[ifile] > 0;
             ifile = (ifile == nfiles - 1 ? 0 : ifile + 1)) {
          std::fstream &fin = fins[ifile];
          header = 0;
          while (header == 0 && fin.good() && sizes[ifile] > 0) {
            fin.read(reinterpret_cast<char *>(&header), sizeof(uint64_t));
            sizes[ifile]--;
          }
          if (header == 0)  // meaning we read nothing but padding
            continue;
          parseHeader(header, run, bx, orbit, good, nwords);
          if (orbit != cmsswOrbit)
            throw std::runtime_error("Orbit mismatch between CMSSW " + std::to_string(cmsswOrbit) +
                                     " and 64 bit header " + std::to_string(orbit) + " from file " + ins[ifile]);
          if (nwords)
            fin.read(reinterpret_cast<char *>(payload), nwords * sizeof(uint64_t));
          sizes[ifile] -= nwords;
          fillEvent(run, orbit, bx, good, nwords, payload);
          ret.entries++;
        }
      } else {  // composite
        for (unsigned int isrc = 0; isrc < nsrcs; ++isrc)
          ifiles[isrc] = sources_[isrc].endOffset - 1;
        for (bx = 1;; ++bx) {
          bool anygood = false, allgood = true;
          for (unsigned int isrc = 0; isrc < nsrcs; ++isrc) {
            Source &src = sources_[isrc];
            ifiles[isrc]++;
            if (ifiles[isrc] == src.endOffset)
              ifiles[isrc] = src.startOffset;
            std::fstream &fin = fins[ifiles[isrc]];
            unsigned int &fsize = sizes[ifiles[isrc]];
            header = 0;
            while (header == 0 && fin.good() && fsize > 0) {
              fin.read(reinterpret_cast<char *>(&header), sizeof(uint64_t));
              fsize--;
            }
            if (header == 0) {  // meaning we read nothing but padding
              allgood = false;
              continue;
            }
            anygood = true;
            parseHeader(header, run, hbx, orbit, good, nwords);
            if (hbx != bx)
              throw std::runtime_error("BX mismatch in source " + std::to_string(isrc) + ", file " +
                                       std::to_string(ifiles[isrc]) + ", bx " + std::to_string(hbx) +
                                       " while expecting " + std::to_string(bx));
            if (orbit != cmsswOrbit)
              throw std::runtime_error("Orbit mismatch between CMSSW " + std::to_string(cmsswOrbit) +
                                       " and 64 bit header " + std::to_string(orbit) + " from file " +
                                       ins[ifiles[isrc]]);
            if (nwords) {
              fin.read(reinterpret_cast<char *>(payload), nwords * sizeof(uint64_t));
              fsize -= nwords;
            }
            src.unpacker->fillEvent(run, orbit, bx, good, nwords, payload, false);
          }  // sources loop
          if (!anygood)
            break;
          if (!allgood)
            throw std::runtime_error("Mismatch between different input files at orbit " + std::to_string(cmsswOrbit));
          fillEvent(run, orbit, bx, allgood, 0, nullptr, true);
          ret.entries++;
        }  // bx loop
      }    // CMSSW headers, composite
      // when we get here, there might still be some trailing zeros in some files
      for (unsigned int ifile = 0; ifile < nfiles; ++ifile) {
        if (fins[ifile].good()) {
          while (sizes[ifile] > 0) {
            header = 0;
            fins[ifile].read(reinterpret_cast<char *>(&header), sizeof(uint64_t));
            sizes[ifile]--;
            if (header != 0)
              std::runtime_error("Trailing non-zero data in file " + std::to_string(ifile) + ins[ifile] + ", orbit " +
                                 std::to_string(cmsswOrbit) + ": " + std::to_string(header));
          }
        }
      }
    }  // outer loop on orbits
  }    // versions with CMSSW headers
  ret.bytes_out = closeOutput();
  ret.time = (std::chrono::duration<double>(std::chrono::steady_clock::now() - tstart)).count();
  return ret;
}

unsigned long int UnpackerBase::unpackOrbits(
    const std::vector<std::pair<const uint64_t *, const uint64_t *>> &buffersIn) {
  unsigned long int entries = 0;
  uint16_t run;
  uint32_t orbit;
  uint16_t bx;
  bool good;
  uint16_t nwords;
  assert(!composite_);  // not yet implemented
  std::vector<std::pair<const uint64_t *, const uint64_t *>> buffers = buffersIn;
  for (int ibuff = 0, nbuffs = buffers.size(), lbuff = nbuffs - 1; buffers[ibuff].first != buffers[ibuff].second;
       ibuff = (ibuff == lbuff ? 0 : ibuff + 1)) {
    auto &pa = buffers[ibuff];
    while (pa.first != pa.second && *pa.first == 0) {
      pa.first++;
    }
    if (pa.first == pa.second)
      continue;
    parseHeader(*pa.first, run, bx, orbit, good, nwords);
    fillEvent(run, orbit, bx, good, nwords, pa.first + 1);
    pa.first += (nwords + 1);
    entries++;
  }

  return entries;
}
