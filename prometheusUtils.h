#ifndef p2_clients_prometheusUtils_h
#define p2_clients_prometheusUtils_h

#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1

  #include <prometheus/counter.h>
  #include <prometheus/exposer.h>
  #include <prometheus/registry.h>
  #include <cstdint>
  #include <thread>
  #include <atomic>
  #include <filesystem>

  #include <sys/time.h>
  #include <sys/resource.h>

class CountersBase {
public:
  CountersBase() : orbits_(0), events_(0), bytes_(0), cntOrbits_(nullptr), cntEvents_(nullptr), cntBytes_(nullptr) {}
  CountersBase(prometheus::Family<prometheus::Counter> &rootOrbits,
               prometheus::Family<prometheus::Counter> &rootEvents,
               prometheus::Family<prometheus::Counter> &rootBytes,
               const prometheus::Labels &labels)
      : orbits_(0),
        events_(0),
        bytes_(0),
        cntOrbits_(&rootOrbits.Add(labels)),
        cntEvents_(&rootEvents.Add(labels)),
        cntBytes_(&rootBytes.Add(labels)) {}
  bool good() const { return cntOrbits_ != nullptr; }
  void update(uint32_t orbits, uint64_t events, uint64_t bytes) {
    cntOrbits_->Increment(double(orbits) - double(orbits_));
    cntEvents_->Increment(double(events) - double(events_));
    cntBytes_->Increment(double(bytes) - double(bytes_));
    orbits_ = orbits;
    events_ = events;
    bytes_ = bytes;
  }

protected:
  uint32_t orbits_;
  uint64_t events_, bytes_;
  prometheus::Counter *cntOrbits_, *cntEvents_, *cntBytes_;
};
class ReceiverCountersSingleStream {
public:
  ReceiverCountersSingleStream() : received_(), truncOrbits_(0), cntTruncatedOrbits_(nullptr) {}
  ReceiverCountersSingleStream(prometheus::Family<prometheus::Counter> &receivedOrbits,
                               prometheus::Family<prometheus::Counter> &receivedEvents,
                               prometheus::Family<prometheus::Counter> &receivedBytes,
                               prometheus::Family<prometheus::Counter> &wroteOrbits,
                               prometheus::Family<prometheus::Counter> &wroteEvents,
                               prometheus::Family<prometheus::Counter> &wroteBytes,
                               prometheus::Family<prometheus::Counter> &wroteFiles,
                               const std::string &streamName)
      : received_(receivedOrbits, receivedEvents, receivedBytes, {{"state", "ok"}, {"stream", streamName}}),
        wrote_(wroteOrbits, wroteEvents, wroteBytes, {{"stream", streamName}}),
        truncOrbits_(0),
        wroteFiles_(0),
        cntTruncatedOrbits_(&receivedOrbits.Add({{"state", "truncated"}, {"stream", streamName}})),
        cntWroteFiles_(&wroteFiles.Add({{"stream", streamName}})) {}
  void update(uint32_t orbits, uint64_t events, uint64_t bytes, uint32_t truncOrbits = 0) {
    if (received_.good()) {
      received_.update(orbits - truncOrbits, events, bytes);
      cntTruncatedOrbits_->Increment(double(truncOrbits) - double(truncOrbits_));
      truncOrbits_ = truncOrbits;
    }
  }
  void updateWrote(uint32_t orbits, uint64_t events, uint64_t bytes, uint32_t files) {
    if (wrote_.good()) {
      wrote_.update(orbits, events, bytes);
      cntWroteFiles_->Increment(double(files) - double(wroteFiles_));
      wroteFiles_ = files;
    }
  }

private:
  CountersBase received_, wrote_;
  uint32_t truncOrbits_, wroteFiles_;
  prometheus::Counter *cntTruncatedOrbits_, *cntWroteFiles_;
};

class ReceiverCountersFactory {
public:
  ReceiverCountersFactory(prometheus::Registry &registry, const std::string &name = "receiver")
      : receivedOrbits_(&prometheus::BuildCounter().Name(name + "_received_orbits_total").Register(registry)),
        receivedEvents_(&prometheus::BuildCounter().Name(name + "_received_events_total").Register(registry)),
        receivedBytes_(&prometheus::BuildCounter().Name(name + "_received_bytes_total").Register(registry)),
        wroteOrbits_(&prometheus::BuildCounter().Name(name + "_wrote_orbits_total").Register(registry)),
        wroteEvents_(&prometheus::BuildCounter().Name(name + "_wrote_events_total").Register(registry)),
        wroteBytes_(&prometheus::BuildCounter().Name(name + "_wrote_bytes_total").Register(registry)),
        wroteFiles_(&prometheus::BuildCounter().Name(name + "_wrote_files_total").Register(registry)) {}

  ReceiverCountersSingleStream make(unsigned int stream) {
    return ReceiverCountersSingleStream(*receivedOrbits_,
                                        *receivedEvents_,
                                        *receivedBytes_,
                                        *wroteOrbits_,
                                        *wroteEvents_,
                                        *wroteBytes_,
                                        *wroteFiles_,
                                        std::to_string(stream));
  }

private:
  prometheus::Family<prometheus::Counter> *receivedOrbits_, *receivedEvents_, *receivedBytes_;
  prometheus::Family<prometheus::Counter> *wroteOrbits_, *wroteEvents_, *wroteBytes_, *wroteFiles_;
};

class CPUCounter {
public:
  static void start(const std::string &prefix, prometheus::Registry &registry, float sampleFrequency = 1) {
    static CPUCounter instance(prefix, registry, sampleFrequency);
    std::thread t(&CPUCounter::update, &instance);
    t.detach();
  }

  void update() {
    struct rusage usage;
    while (true) {
      if (getrusage(RUSAGE_SELF, &usage) == 0) {
        double user = usage.ru_utime.tv_sec + usage.ru_utime.tv_usec / 1e6;
        double sys = usage.ru_stime.tv_sec + usage.ru_stime.tv_usec / 1e6;
        cntUser_->Increment(user - user_);
        cntUser_->Increment(sys - sys_);
        user_ = user;
        sys_ = sys;
        std::this_thread::sleep_for(std::chrono::milliseconds(frequencyInMs_));
      }
    }
  }

private:
  double user_, sys_;
  unsigned frequencyInMs_;
  prometheus::Counter *cntUser_, *cntSys_;

  CPUCounter(const std::string &prefix, prometheus::Registry &registry, float sampleFrequency = 1)
      : user_(0), sys_(0), frequencyInMs_(1000 * sampleFrequency) {
    prometheus::Family<prometheus::Counter> &root =
        prometheus::BuildCounter().Name(prefix + "_cpu_seconds_total").Help("CPU usage in seconds").Register(registry);
    cntUser_ = &root.Add({{"type", "user"}});
    cntSys_ = &root.Add({{"type", "system"}});
  }
};

class DirGauge {
public:
  static void start(const std::string &prefix,
                    const std::string &path,
                    bool recursive,
                    prometheus::Registry &registry,
                    float sampleFrequency = 3) {
    static DirGauge instance(prefix, path, recursive, registry, sampleFrequency);
    std::thread t(&DirGauge::update, &instance);
    t.detach();
  }

  void update() {
    std::filesystem::path root{path_};
    while (true) {
      unsigned nfiles = 0;
      unsigned long bytes = 0;
      if (recursive_) {
        for (auto const &f : std::filesystem::recursive_directory_iterator{root})
          add(f, nfiles, bytes);
      } else {
        for (auto const &f : std::filesystem::directory_iterator{root})
          add(f, nfiles, bytes);
      }
      cntFiles_->Set(nfiles);
      cntBytes_->Set(bytes);
      std::this_thread::sleep_for(std::chrono::milliseconds(frequencyInMs_));
    }
  }

  void add(const std::filesystem::directory_entry &f, unsigned &nfiles, unsigned long &bytes) {
    std::error_code ec;
    if (f.is_regular_file(ec) && ec.value() == 0) {
      nfiles++;
      auto size = f.file_size(ec);
      if (ec.value() == 0)
        bytes += size;
    }
  }

private:
  const std::string path_;
  bool recursive_;
  unsigned frequencyInMs_;
  prometheus::Gauge *cntFiles_, *cntBytes_;

  DirGauge(const std::string &prefix,
           const std::string &path,
           bool recursive,
           prometheus::Registry &registry,
           float sampleFrequency)
      : path_(path), recursive_(recursive), frequencyInMs_(1000 * sampleFrequency) {
    cntFiles_ = &prometheus::BuildGauge().Name(prefix + "_files").Help("Number of files").Register(registry).Add({});
    cntBytes_ = &prometheus::BuildGauge().Name(prefix + "_bytes").Help("Sum of file sizes").Register(registry).Add({});
  }
};

#else
// dummy classes
class ReceiverCountersSingleStream {
public:
  ReceiverCountersSingleStream() {}
  void update(uint32_t, uint64_t, uint64_t) {}
  void update(uint32_t, uint64_t, uint64_t, uint32_t) {}
  void updateWrote(uint32_t, uint64_t, uint64_t, uint32_t) {}
};

#endif

#endif