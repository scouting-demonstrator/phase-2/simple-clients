#ifndef p2_clients_unpacker_base_h
#define p2_clients_unpacker_base_h
#include <cstdio>
#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <memory>

class UnpackerBase {
public:
  struct Report {
    unsigned long int entries;
    float bytes_in;
    float bytes_out;
    float time;
    explicit Report(unsigned long int e, float t = 0, float i = 0, float o = 0)
        : entries(e), bytes_in(i), bytes_out(o), time(t) {}
    Report &operator+=(const Report &other) {
      entries += other.entries;
      bytes_in += other.bytes_in;
      bytes_out += other.bytes_out;
      time += other.time;
      return *this;
    }
  };
  struct Spec {
    enum class ObjType { Puppi, TkEm, TkMu, Composite };
    ObjType objType;
    std::string fileKind;
    std::string format;
    typedef std::map<std::string, std::string> Options;
    Options options;
    Spec(const std::string &obj,  // ObjType, fully lowercase
         const std::string &kind,
         const std::string &unpackFormat,
         const Options &options = {});
    Spec(const ObjType &obj, const std::string &kind, const std::string &unpackFormat, const Options &options = {})
        : objType(obj), fileKind(kind), format(unpackFormat), options(options) {}
    int intOption(const std::string &name, int defaultValue = 0) const {
      auto match = options.find(name);
      return match != options.end() ? std::stoi(match->second) : defaultValue;
    }
    bool boolOption(const std::string &name, bool defaultValue = false) const {
      auto match = options.find(name);
      return match != options.end() ? (match->second == "true") : defaultValue;
    }
    void setBoolOption(const std::string &name, bool value = true) { options[name] = (value ? "true" : "false"); }
    const std::string &strOption(const std::string &name, const std::string &defaultValue) const {
      auto match = options.find(name);
      return match != options.end() ? match->second : defaultValue;
    }
    bool hasOption(const std::string &name) const {
      auto match = options.find(name);
      return match != options.end();
    }
    const std::string &compressionAlgo() const { return strOption("compressionAlgo", "none"); }
    int compressionLevel() const { return intOption("compressionLevel"); }
    void setCompression(const std::string &algo, int level = 5) {
      options["compressionAlgo"] = algo;
      options["compressionLevel"] = std::to_string((algo == "none") ? 0 : level);
    }
    bool cmsswHeaders() const { return boolOption("cmsswHeaders"); }
    void setCMSSWHeaders(bool headers = true) { return setBoolOption("cmsswHeaders", headers); }
    int numThreads() const { return intOption("numThreads"); }
    void setThreads(int threads) { options["numThreads"] = std::to_string(threads); }
  };
  UnpackerBase(const Spec &spec);
  virtual ~UnpackerBase();
  virtual Report unpackFiles(const std::vector<std::string> &ins, const std::string &out);
  unsigned long int unpackOrbits(const std::vector<std::pair<const uint64_t *, const uint64_t *>> &buffers);
  virtual void bookOutput(const std::string & /*out*/) = 0;
  virtual unsigned long int closeOutput() { return 0; }
  virtual void fillEvent(uint16_t /*run*/,
                         uint32_t /*orbit*/,
                         uint16_t /*bx*/,
                         bool /*good*/,
                         uint16_t /*nwords*/,
                         const uint64_t * /*words*/,
                         bool commit = true) = 0;
  virtual std::string outputExtension() = 0;

protected:
  bool cmsswHeaders_, composite_;
  // for composite implementations
  struct Source {
    Source(uint16_t nTimeslices, uint16_t offset, std::unique_ptr<UnpackerBase> anUnpacker)
        : timeslices(nTimeslices),
          startOffset(offset),
          endOffset(offset + nTimeslices),
          unpacker(std::move(anUnpacker)) {}
    uint16_t timeslices, startOffset, endOffset;
    std::unique_ptr<UnpackerBase> unpacker;
  };
  std::vector<Source> sources_;
};

#endif