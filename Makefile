CC = c++
CCFLAGS = --std=c++17 -march=x86-64 -msse4.2 -mavx2 -mfma -mf16c -W -Wall  -Ofast -ggdb
LIBS = -lstdc++ -lstdc++fs -pthread
.PHONY: clean install format run_tests env envlcg
USE_ROOT ?= 1
USE_APACHE ?= 1
USE_PROMETHEUS ?= 0

UNPACKERS_CFLAGS :=
UNPACKERS_LIBDEPS := libunpackerBase.so
UNPACKERS_LIBS := -L. -lunpackerBase

LIVE_INC :=
LIVE_CCFLAGS :=
LIVE_CLIBS :=

INSTALL_PREFIX ?= /usr/local

TARGETS := p2scout-client p2scout-generator p2scout-fileBroker p2scout-fileWaiter 
ifeq ($(or $(USE_ROOT),$(USE_APACHE)), 1)
	TARGETS += p2scout-unpacker p2scout-liveUnpacker libunpackerBase.so libunpacker.so
endif

ifeq ($(USE_ROOT), 1)
	CCFLAGS += -DUSE_ROOT=1
	vpath %.so root
	UNPACKERS_LIBDEPS += librootUnpacker.so
	UNPACKERS_LIBS += -Lroot -lrootUnpacker $(shell root-config --ldflags --libs) 
	UNPACKERS_CFLAGS += $(shell root-config --cflags)
endif

ifeq ($(USE_APACHE), 1)
	CCFLAGS += -DUSE_APACHE=1
	vpath %.so apache
	UNPACKERS_LIBDEPS += libapacheUnpacker.so
	UNPACKERS_LIBS += -Lapache -lapacheUnpacker
	TARGETS += p2scout-unpacker p2scout-liveUnpacker libunpackerBase.so libunpacker.so
endif

ifeq ($(USE_PROMETHEUS), 1)
	LIVE_INC += prometheusUtils.h
ifdef PROMETHEUS
	LIVE_CCFLAGS += -DUSE_PROMETHEUS=1 -I$(PROMETHEUS)/core/include -I$(PROMETHEUS)/pull/include
	LIVE_LIBS += -L$(PROMETHEUS)/lib -lprometheus-cpp-pull -lprometheus-cpp-core -lz
else
	LIVE_CCFLAGS += -DUSE_PROMETHEUS=1 
	LIVE_LIBS += -lprometheus-cpp-pull -lprometheus-cpp-core -lz
endif
endif

ifdef TBB
	LIVE_CCFLAGS += -I$(TBB)/include
	LIVE_LIBS += -L$(TBB)/lib
endif

all: $(TARGETS)

envlcg106:
	@echo 'source /cvmfs/sft.cern.ch/lcg/views/LCG_106/x86_64-el9-gcc13-opt/setup.sh;'
	@echo 'export TBB=/cvmfs/sft.cern.ch/lcg/views/LCG_106/x86_64-el9-gcc13-opt;'
	@echo 'export LD_LIBRARY_PATH=$$PWD:$${LD_LIBRARY_PATH};'
ifeq ($(USE_ROOT), 1)
	@echo 'export LD_LIBRARY_PATH=$$PWD/root:$${LD_LIBRARY_PATH};'
endif
ifeq ($(USE_APACHE), 1)
	@echo 'export ARROW_INCLUDE=/cvmfs/sft.cern.ch/lcg/views/LCG_106/x86_64-el9-gcc13-opt/include;'
	@echo 'export ARROW_LIB=/cvmfs/sft.cern.ch/lcg/views/LCG_106/x86_64-el9-gcc13-opt/lib64;'
	@echo 'export LD_LIBRARY_PATH=$$PWD/apache:$${LD_LIBRARY_PATH};'
endif

envlcgdev:
	@echo 'source /cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-el9-gcc13-opt/setup.sh;'
	@echo 'export TBB=/cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-el9-gcc13-opt;'
	@echo 'export LD_LIBRARY_PATH=$$PWD:$${LD_LIBRARY_PATH};'
ifeq ($(USE_ROOT), 1)
	@echo 'export LD_LIBRARY_PATH=$$PWD/root:$${LD_LIBRARY_PATH};'
endif
ifeq ($(USE_APACHE), 1)
	@echo 'export ARROW_INCLUDE=/cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-el9-gcc13-opt/include;'
	@echo 'export ARROW_LIB=/cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-el9-gcc13-opt/lib64;'
	@echo 'export LD_LIBRARY_PATH=$$PWD/apache:$${LD_LIBRARY_PATH};'
endif

env:
	@echo 'export LD_LIBRARY_PATH=$$PWD:$${LD_LIBRARY_PATH};'
ifeq ($(USE_ROOT), 1)
	@echo 'export LD_LIBRARY_PATH=$$PWD/root:$${LD_LIBRARY_PATH};'
endif
ifeq ($(USE_APACHE), 1)
	@echo 'export LD_LIBRARY_PATH=$$PWD/apache:$${LD_LIBRARY_PATH};'
endif

librootUnpacker.so: libunpackerBase.so
	@cd root && $(MAKE) librootUnpacker.so

libapacheUnpacker.so: libunpackerBase.so
	@cd apache && $(MAKE) libapacheUnpacker.so

unpack.o: unpack.cc unpack.h UnpackerBase.h
	$(CC) -fPIC $(CCFLAGS) -c $< -o $@

UnpackerBase.o: UnpackerBase.cc unpack.h UnpackerBase.h
	$(CC) -fPIC $(CCFLAGS) -c $< -o $@

UnpackMaker.o: UnpackMaker.cc UnpackMaker.h
	$(CC) -fPIC $(CCFLAGS) -c $< $(LIBS) -o $@

libunpackerBase.so: UnpackerBase.o unpack.o
	$(CC) $(CCFLAGS) $^ -shared $(LIBS) -o $@

libunpacker.so: UnpackMaker.o $(UNPACKERS_LIBDEPS)
	$(CC) $(CCFLAGS) $< -shared $(LIBS) $(UNPACKERS_LIBS) -o $@

% : %.cc
	$(CC) $(CCFLAGS) $< $(LIBS) -o $@

p2scout-client: p2scout-client.cc $(LIVE_INC)
	$(CC) $(CCFLAGS) $(LIVE_CCFLAGS) $< $(LIBS) $(LIVE_LIBS) -o $@

RECEIVER_OBJS := source.o orbitReceiver.o orbitSink.o
$(RECEIVER_OBJS): %.o: %.cc %.h
	$(CC) -c $(CCFLAGS) $< $(LIBS) -o $@

receiver: receiver.cc $(RECEIVER_OBJS)
	$(CC) $(CCFLAGS) $< $(RECEIVER_OBJS) $(LIBS) -o $@  -I$(TBB)/include -L$(TBB)/lib -ltbb

receive256tbb : receive256tbb.cc
	$(CC) $(CCFLAGS) $< $(LIBS) -o $@ -I$(TBB)/include -L$(TBB)/lib -ltbb

p2scout-unpacker: UnpackMain.cc $(UNPACKERS_LIBDEPS) libunpacker.so
	$(CC) $(CCFLAGS) $< $(LIBS) $(UNPACKERS_LIBS) -lunpacker -o $@

p2scout-liveUnpacker: liveUnpacker.cc $(UNPACKERS_LIBDEPS) libunpacker.so $(LIVE_INC)
	$(CC) $(CCFLAGS) $(LIVE_CCFLAGS) $(UNPACKERS_CFLAGS) $< $(LIBS) $(LIVE_LIBS) $(UNPACKERS_LIBS) -lunpacker -ltbb -o $@

p2scout-fileWaiter : utils/file_waiter.cc
	$(CC) $(CCFLAGS) $< $(LIBS) -o $@

p2scout-fileBroker: p2scout-fileBroker.cc $(LIVE_INC)
	$(CC) $(CCFLAGS) $(LIVE_CCFLAGS) $< $(LIBS) $(LIVE_LIBS) -o $@

format:
	clang-format -i *.cc *.h utils/*.cc 
	@cd apache && $(MAKE) format
	@cd root && $(MAKE) format

clean:
	@rm $(TARGETS) *.data *.o *.so 2> /dev/null || true
	@rm -r received_raw 2> /dev/null || true
ifeq ($(USE_ROOT), 1)
	@cd root && $(MAKE) clean > /dev/null
endif
ifeq ($(USE_APACHE), 1)
	@cd apache && $(MAKE) clean > /dev/null
endif

run_tests: p2scout-client p2scout-generator
	@echo "Running basic file tests"
	./p2scout-client DTHBasic data/tcp_genx4.data
	./p2scout-client DTHBasicOA data/tcp_genx4_OA.data
	bash -c "(sleep 1 && cat data/tcp_genx4_OA.data > /dev/tcp/127.0.0.1/9988 &)"
	./p2scout-client DTHBasicOA 127.0.0.1:9988
	./p2scout-client DTHBasic256 data/tcp_gen4_dth256.data
	bash -c "(sleep 1 && cat data/tcp_gen4_dth256.data > /dev/tcp/127.0.0.1/9988 &)"
	./p2scout-client DTHReceive256 127.0.0.1:9988 native64sz.data
	./p2scout-client Native64SZ native64sz.data
	./p2scout-generator Native64 root/data/Puppi.dump native64sz.data --orbits 100 && ./p2scout-client Native64 native64sz.data
	./p2scout-generator DTHBasic256 root/data/Puppi.dump dth256.data --orbits 100 && ./p2scout-client DTHBasic256 dth256.data
	bash -c "(sleep 1 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync &)"
	./p2scout-client DTHBasic256 127.0.0.1:9988 -n 2
	./p2scout-generator CMSSW root/data/Puppi.dump cmssw.data --orbits 100 && ./p2scout-client CMSSW cmssw.data
	rm -r received_raw 2> /dev/null || true
	bash -c "(sleep 1 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync &)"
	./p2scout-client DTHRollingReceive256 127.0.0.1:9988 received_raw --orbitBitsPerFile 9 -n 2
	./p2scout-client Native64SZ received_raw/run000000/run000000_ls0001_index000001_stream00.raw -t 0 
	./p2scout-client Native64SZ received_raw/run000000/run000000_ls0001_index000001_stream01.raw -t 1 
	./p2scout-client Native64SZ received_raw/run000000/run000000_ls0001_index000513_stream00.raw -t 0 
	./p2scout-client Native64SZ received_raw/run000000/run000000_ls0001_index000513_stream01.raw -t 1 
	bash -c "(sleep 1 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 --sync &)"
	./p2scout-client DTHRollingReceive256 127.0.0.1:9988 received_raw --orbitBitsPerFile 9 --cmssw  --r 37 -i 8
	./p2scout-client CMSSW received_raw/run000037/run000037_ls0001_index000001_stream08.raw 
	./p2scout-client CMSSW received_raw/run000037/run000037_ls0001_index000513_stream08.raw 
	bash -c "(sleep 1 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync &)"
	./p2scout-client DTHRollingReceive256MM 127.0.0.1:9988 received_raw2 --orbitBitsPerFile 9 -n 2
	./p2scout-client Native64SZ received_raw2/run000000/run000000_ls0001_index000001_stream00.raw -t 0 
	./p2scout-client Native64SZ received_raw2/run000000/run000000_ls0001_index000001_stream01.raw -t 1 
	./p2scout-client Native64SZ received_raw2/run000000/run000000_ls0001_index000513_stream00.raw -t 0 
	./p2scout-client Native64SZ received_raw2/run000000/run000000_ls0001_index000513_stream01.raw -t 1 
	bash -c "(sleep 1 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 --sync &)"
	./p2scout-client DTHRollingReceive256MM 127.0.0.1:9988 received_raw2 --orbitBitsPerFile 9 --cmssw  --r 37 -i 8
	./p2scout-client CMSSW received_raw2/run000037/run000037_ls0001_index000001_stream08.raw 
	./p2scout-client CMSSW received_raw2/run000037/run000037_ls0001_index000513_stream08.raw 
	rm -r received_raw received_raw2 2> /dev/null || true

LIVEDIR ?= /dev/shm/$$UID/p2scout
run_test_live:
ifeq ($(USE_ROOT), 1)
	@echo "== Testing with iNotify =="
	bash -c "(sleep 2 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync > /dev/null &)"
	bash -c "(./p2scout-client --cmssw DTHRollingReceive256 127.0.0.1:9988 $(LIVEDIR)/raw --orbitBitsPerFile 9 --run 37 -n 2  > /dev/null &)"
	./p2scout-liveUnpacker --cmssw puppi ttree float $(LIVEDIR)/raw/run000037  $(LIVEDIR)/root/run000037
	@ls -l $(LIVEDIR)/root/run000037 | awk 'BEGIN{found=0} /run000037_ls0001_index.*_stream0[01].root/{if ($$5 >= 240e6) found = found+1; print; } END{print "found "found" good files."; if (found != 4) exit 1; }'
	@rm -r $(LIVEDIR)/raw/run000037 $(LIVEDIR)/root/run000037/*.root
	bash -c "(sleep 2 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync > /dev/null &)"
	bash -c "(./p2scout-client --cmssw DTHRollingReceive256 127.0.0.1:9988 $(LIVEDIR)/raw --orbitBitsPerFile 9 --run 37 -n 2 > /dev/null &)"
	./p2scout-liveUnpacker --cmssw puppi ttree float $(LIVEDIR)/raw/run000037  $(LIVEDIR)/root/run000037 --demux 2
	@rm -r $(LIVEDIR)/raw/run000037 $(LIVEDIR)/root/run000037/*.root
	@echo "== Testing with a fileBroker =="
	bash -c "(sleep 2 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync > /dev/null &)"
	bash -c "(./p2scout-client --cmssw DTHRollingReceive256 127.0.0.1:9988 $(LIVEDIR)/raw --orbitBitsPerFile 9 --run 37 -n 2  > /dev/null &)"
	bash -c "(./p2scout-fileBroker -a 127.0.0.1 -p 9999 $(LIVEDIR)/raw/run000037 --stream0  > /dev/null &)"
	./p2scout-liveUnpacker --cmssw puppi ttree float 127.0.0.1:9999 $(LIVEDIR)/raw/run000037  $(LIVEDIR)/root/run000037 --run 37  --demux 2
	@ls -l $(LIVEDIR)/root/run000037 | awk 'BEGIN{found=0} /run000037_ls0001_index[0-9]*.root/{if ($$5 >= 450e6) found = found+1; print; } END{print "found "found" good files."; if (found != 2) exit 1; }'
	@rm -r $(LIVEDIR)/raw/run000037 $(LIVEDIR)/root/run000037/*.root
endif
ifeq ($(USE_APACHE), 1)
	bash -c "(sleep 2 && ./p2scout-generator DTHBasic256 root/data/Puppi.dump 127.0.0.1:9988 --orbits 1000 -n 2 --sync > /dev/null &)"
	bash -c "(./p2scout-client --cmssw DTHRollingReceive256 127.0.0.1:9988 $(LIVEDIR)/raw --orbitBitsPerFile 9 --run 37 -n 2  > /dev/null &)"
	./p2scout-liveUnpacker --cmssw puppi ipcstream float $(LIVEDIR)/raw/run000037  $(LIVEDIR)/apache/run000037
	@ls -l $(LIVEDIR)/apache/run000037 | awk 'BEGIN{found=0} /run000037_ls0001_index.*_stream0[01].arrow/{if ($$5 >= 230e6) found = found+1; print; } END{print "found "found" good files."; if (found != 4) exit 1; }'
	@rm -r $(LIVEDIR)/raw/run000037 $(LIVEDIR)/apache/run000037/*.arrow
endif	

install: $(TARGETS)
	@install -v p2scout-client $(INSTALL_PREFIX)/bin/
	@install -v p2scout-generator $(INSTALL_PREFIX)/bin/
	@install -v p2scout-fileWaiter $(INSTALL_PREFIX)/bin/
	@install -v p2scout-fileBroker $(INSTALL_PREFIX)/bin/
ifeq ($(or $(USE_ROOT),$(USE_APACHE)), 1)
	@install -v p2scout-unpacker $(INSTALL_PREFIX)/bin/
	@install -v p2scout-liveUnpacker $(INSTALL_PREFIX)/bin/
	@install -v libunpackerBase.so $(INSTALL_PREFIX)/lib64/
	@install -v libunpacker.so $(INSTALL_PREFIX)/lib64/
endif
ifeq ($(USE_ROOT), 1)
	@cd root && $(MAKE) install
endif
ifeq ($(USE_APACHE), 1)
	@cd apache && $(MAKE) install
endif	
