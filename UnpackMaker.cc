#include "UnpackMaker.h"
#include <stdexcept>

#ifdef USE_ROOT
  #include "root/RootUnpackMaker.h"
#endif

#ifdef USE_APACHE
  #include "apache/ApacheUnpackMaker.h"
#endif

std::unique_ptr<UnpackerBase> UnpackMaker::make(const Spec &spec) {
#ifdef USE_ROOT
  if (RootUnpackMaker::accept(spec.fileKind)) {
    return RootUnpackMaker::make(spec);
  }
#endif
#ifdef USE_APACHE
  if (ApacheUnpackMaker::accept(spec.fileKind)) {
    return ApacheUnpackMaker::make(spec);
  }
#endif
  throw std::invalid_argument("UnpackMaker: can't make an unpacker with fileKind '" + spec.fileKind + "'");
  return std::unique_ptr<UnpackerBase>();
}

bool UnpackMaker::is_output_fname(const std::string &fname) {
  bool ret = false;
#ifdef USE_ROOT
  ret |= (fname.length() > 5 && fname.substr(fname.length() - 5) == ".root");
#endif
#ifdef USE_APACHE
  ret |= ApacheUnpackMaker::is_output_fname(fname);
#endif
  return ret;
}