#ifndef p2_clients_apache_ArrowUnpackerRaw64_h
#define p2_clients_apache_ArrowUnpackerRaw64_h
#include "ArrowUnpackerBase.h"

class ArrowUnpackerRaw64 : public ArrowUnpackerBase {
public:
  ArrowUnpackerRaw64(const Spec& spec);
  ~ArrowUnpackerRaw64() {}
  void unpackAndCommitBatch() override;

protected:
  std::shared_ptr<arrow::DataType> puppisType_;
  std::shared_ptr<arrow::Field> puppiField_;
};

#endif