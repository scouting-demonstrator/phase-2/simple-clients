#include "ArrowUnpackerBase.h"
#include <cassert>
#include <stdexcept>
#include <filesystem>
#include <fstream>
#include <chrono>
#include "../unpack.h"

ArrowUnpackerBase::ArrowUnpackerBase(const Spec &spec, const std::string &objName)
    : UnpackerBase(spec),
      batchsize_(spec.intOption("batchSize", DefaultBatchSize)),
      fileKind_(spec.fileKind),
      compressionMethod_(spec.compressionAlgo()),
      compressionLevel_(spec.compressionLevel()),
      runField_(arrow::field("run", arrow::uint16())),
      orbitField_(arrow::field("orbit", arrow::uint32())),
      bxField_(arrow::field("bx", arrow::uint16())),
      goodField_(arrow::field("good" + objName, arrow::boolean())),
      run_(batchsize_),
      bx_(batchsize_),
      orbit_(batchsize_),
      goodBuilder_(std::make_shared<arrow::BooleanBuilder>()),
      nwords_(batchsize_),
      offsets_(1, 0) {
  if (spec.fileKind == ApacheUnpackMaker::FileKind::IPCStream ||
      spec.fileKind == ApacheUnpackMaker::FileKind::IPCFile) {
    // ok
  } else if (spec.fileKind == ApacheUnpackMaker::FileKind::Parquet) {
#ifndef USE_PARQUET
    throw std::invalid_argument("Support for Parquet was not compiled into this version of the unpacker");
#endif
  } else {
    throw std::invalid_argument("Unsupported arrow filekind " + spec.fileKind);
  }
  if (spec.numThreads() > 0)
    arrow::SetCpuThreadPoolCapacity(spec.numThreads());
  if (spec.hasOption("iothreads"))
    arrow::io::SetIOThreadPoolCapacity(spec.intOption("iothreads"));
}

void ArrowUnpackerBase::bookOutput(const std::string &out) {
  if (!out.empty()) {
    assert(fout_.empty());
    fout_ = out;
    if (fileKind_ == ApacheUnpackMaker::FileKind::IPCStream || fileKind_ == ApacheUnpackMaker::FileKind::IPCFile) {
      if (out.length() <= 7 || out.substr(out.length() - 6) != ".arrow") {
        fout_ = out + ".arrow";
      }
    } else if (fileKind_ == ApacheUnpackMaker::FileKind::Parquet) {
      if (out.length() <= 9 || out.substr(out.length() - 8) != ".parquet") {
        fout_ = out + ".parquet";
      }
    }
    outputFile_ = *arrow::io::FileOutputStream::Open(fout_);
    if (fileKind_ == ApacheUnpackMaker::FileKind::IPCStream || fileKind_ == ApacheUnpackMaker::FileKind::IPCFile) {
      arrow::ipc::IpcWriteOptions ipcWriteOptions = arrow::ipc::IpcWriteOptions::Defaults();
      if (compressionMethod_ == "lz4") {
        ipcWriteOptions.codec = *arrow::util::Codec::Create(arrow::Compression::LZ4_FRAME, compressionLevel_);
      } else if (compressionMethod_ == "zstd") {
        ipcWriteOptions.codec = *arrow::util::Codec::Create(arrow::Compression::ZSTD, compressionLevel_);
      } else if (compressionMethod_ != "none") {
        throw std::invalid_argument("Unknown compression " + compressionMethod_);
      }
      if (fileKind_ == ApacheUnpackMaker::FileKind::IPCStream) {
        batchWriter_ = *arrow::ipc::MakeStreamWriter(outputFile_, schema_, ipcWriteOptions);
      } else if (fileKind_ == ApacheUnpackMaker::FileKind::IPCFile) {
        batchWriter_ = *arrow::ipc::MakeFileWriter(outputFile_, schema_, ipcWriteOptions);
      }
    }
  } else if (fileKind_ == ApacheUnpackMaker::FileKind::Parquet) {
#ifdef USE_PARQUET
    // Choose compression
    std::shared_ptr<parquet::WriterProperties> props;
    auto builder = parquet::WriterProperties::Builder();
    arrow::ipc::IpcWriteOptions ipcWriteOptions = arrow::ipc::IpcWriteOptions::Defaults();
    if (compressionMethod_ == "lz4") {
      props = builder.compression(arrow::Compression::LZ4_HADOOP)->compression_level(compressionLevel_)->build();
    } else if (compressionMethod_ == "zlib") {
      props = builder.compression(arrow::Compression::GZIP)->compression_level(compressionLevel_)->build();
    } else if (compressionMethod_ == "zstd") {
      props = builder.compression(arrow::Compression::ZSTD)->compression_level(compressionLevel_)->build();
    } else if (compressionMethod_ == "snappy") {
      props = builder.compression(arrow::Compression::SNAPPY)->compression_level(compressionLevel_)->build();
    } else if (compressionMethod_ != "none") {
      props = builder.build();
    } else if (compressionMethod_ != "none") {
      throw std::invalid_argument("Unknown compression " + compressionMethod_);
    }
    //std::shared_ptr<parquet::ArrowWriterProperties> arrow_props =
    //    parquet::ArrowWriterProperties::Builder().store_schema()->build();
    std::shared_ptr<parquet::ArrowWriterProperties> arrow_props = parquet::ArrowWriterProperties::Builder().build();
    parquetWriter_ =
        *parquet::arrow::FileWriter::Open(*schema_, arrow::default_memory_pool(), outputFile_, props, arrow_props);
#else
    throw std::invalid_argument("Support for Parquet was not compiled into this version of the unpacker");
#endif
  }
  entriesInBatch_ = 0;
  batches_ = 0;
}

unsigned long int ArrowUnpackerBase::closeOutput() {
  unsigned long int ret = 0;
  if (entriesInBatch_)
    unpackAndCommitBatch();
  if (outputFile_) {
    if (batchWriter_)
      batchWriter_->Close();
    outputFile_->Close();
    ret = std::filesystem::file_size(fout_);
    fout_.clear();
  }
  return ret;
}