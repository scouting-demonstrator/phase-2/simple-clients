#include "ApacheUnpackMaker.h"

#include "ArrowUnpackerBase.h"
#include "ArrowUnpackerFloats.h"
#include "ArrowUnpackerInts.h"
#include "ArrowUnpackerRaw64.h"
#include "ArrowUnpackerTkMuFloats.h"
#include <arrow/io/api.h>

std::unique_ptr<UnpackerBase> ApacheUnpackMaker::make(const ApacheUnpackMaker::Spec &spec) {
  std::unique_ptr<UnpackerBase> unpacker;
  switch (spec.objType) {
    case Spec::ObjType::Puppi:
      if (spec.fileKind == FileKind::IPCStream || spec.fileKind == FileKind::IPCFile ||
          spec.fileKind == FileKind::Parquet) {
        if (spec.format == "float" || spec.format == "float16") {
          unpacker = std::make_unique<ArrowUnpackerFloats>(spec);
        } else if (spec.format == "int") {
          unpacker = std::make_unique<ArrowUnpackerInts>(spec);
        } else if (spec.format == "raw64") {
          unpacker = std::make_unique<ArrowUnpackerRaw64>(spec);
        } else {
          throw std::invalid_argument("Unsupported puppi format " + spec.format);
        }
      } else {
        throw std::invalid_argument("Unsupported puppi fileKind " + spec.fileKind);
      }
      break;
    case Spec::ObjType::TkEm:
      throw std::invalid_argument("Unsupported apache objType 'tkem'");
      break;
    case Spec::ObjType::TkMu:
      if (spec.fileKind == FileKind::IPCStream || spec.fileKind == FileKind::IPCFile ||
          spec.fileKind == FileKind::Parquet) {
        if (spec.format == "float" || spec.format == "float16") {
          unpacker = std::make_unique<ArrowUnpackerTkMuFloats>(spec);
        } else {
          throw std::invalid_argument("Unsupported tkmu ipc format " + spec.format);
        }
      } else {
        throw std::invalid_argument("Unsupported puppi fileKind " + spec.fileKind);
      }
      break;
    case Spec::ObjType::Composite:
      throw std::invalid_argument("Unsupported apache objType 'composite'");
      break;
  }
  return unpacker;
}

bool ApacheUnpackMaker::is_output_fname(const std::string &fname) {
  if (fname.length() > 6 && fname.substr(fname.length() - 6) == ".arrow")
    return true;
  if (fname.length() > 4 && fname.substr(fname.length() - 4) == ".ipc")
    return true;
  if (fname.length() > 8 && fname.substr(fname.length() - 8) == ".parquet")
    return true;
  return false;
}