# Apache Arrow unpackers for Phase-2 L1 Data Scouting formats

All the tools here read data in **Native64**` format and unpack it [Apache Arrow](https://arrow.apache.org/docs/cpp/index.html) native IPC format. A sample input data file can be found in the `root/data` directory upstream.

```bash
p2scout-unpacker <objType> <fileType> <format> inputs.dump [output.arrow]
```
The unpackers produce a RecordBatch where the Puppi objects are saved as a List of Struct with the data contents saved as `float`, `float16`, or integers; a version that also just saves puppi candidates as raw 64 bit numbers is available. The batch size defaults to 3564 bunch crossings (1 orbit), but can be configured with the option `-b` or `--batchsize`.

The full list of _objType_ supported object types and _format_ is
 * `puppi`: `float`, `float16`, `int`, `raw64`
 * `tkmu`: `float`, `float16`

The _fileType_ can be `ipcstream` or `ipcfile`. A `parquet` format is partially implemented but not yet working, and disabled at compile time.

A simple validation can be run with `make run_tests`, and some performance studies with `DATAFILE=/path/to/file make run_test_speed`

The code is tested with Apache Arrow 17.0, but should work also with older releases.