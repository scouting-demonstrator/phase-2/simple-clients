#ifndef p2_clients_apache_ApacheUnpackMaker_h
#define p2_clients_apache_ApacheUnpackMaker_h
#include "../UnpackerBase.h"
#include <memory>
#include <string_view>

namespace ApacheUnpackMaker {
  struct FileKind {
    static constexpr std::string_view IPCStream = "ipcstream";
    static constexpr std::string_view IPCFile = "ipcfile";
    static constexpr std::string_view Parquet = "parquet";
  };
  typedef UnpackerBase::Spec Spec;

  std::unique_ptr<UnpackerBase> make(const Spec &spec);

  bool is_output_fname(const std::string &fname);

  inline bool accept(const std::string &fileKind) {
    return fileKind == FileKind::IPCStream || fileKind == FileKind::IPCFile || fileKind == FileKind::Parquet;
  }
}  // namespace ApacheUnpackMaker

#endif