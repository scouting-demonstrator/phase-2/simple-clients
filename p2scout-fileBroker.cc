#include <algorithm>
#include <arpa/inet.h>
#include <atomic>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <deque>
#include <exception>
#include <fcntl.h>
#include <filesystem>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <list>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <regex>
#include <sstream>
#include <string.h>
#include <string>
#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <vector>

#include "prometheusUtils.h"

class SimpleBroker {
public:
  SimpleBroker(unsigned int timeout)
      : timeout_(timeout), state_("STARTING"), donels_(0), maxls_(0), completed_(false) {}
  void push(const std::string& file, unsigned int ls) {
    const std::lock_guard<std::mutex> lock(mutex_);
    items_.emplace_back(file, ls);
    maxls_ = ls;
    state_ = "READY";
    last_ = std::chrono::steady_clock::now();
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
    if (cntFound) {
      cntFound->Increment(1);
      cntSize->Set(items_.size());
    }
#endif
  }
  bool completed() const { return completed_; }
  double staleTime() const { return std::chrono::duration<double>(std::chrono::steady_clock::now() - last_).count(); }
  bool pop(std::string& state, std::string& file, unsigned int& ls, unsigned int& eols) {
    const std::lock_guard<std::mutex> lock(mutex_);
    if (items_.empty()) {
      ls = maxls_;
      file.clear();
      if (state_ != "STARTING") {
        if (staleTime() > timeout_) {
          state_ = (state_ == "EOLS") ? "EOR" : "EOLS";
          if (state_ == "EOR" && staleTime() > 3 * timeout_) {
            completed_ = true;
          }
        }
      }
      state = state_;
      eols = donels_;
      return false;
    } else {
      auto it = items_.front();
      items_.pop_front();
      file = it.file;
      ls = it.ls;
      donels_ = ls - 1;
      state = state_;
      eols = donels_;
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
      if (cntServed) {
        cntServed->Increment(1);
        cntSize->Set(items_.size());
      }
#endif
      return true;
    }
  }

#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
  void setUpPrometheus(prometheus::Registry& registry) {
    cntFound = &prometheus::BuildCounter().Name("fileBroker_files_found_total").Register(registry).Add({});
    cntServed = &prometheus::BuildCounter().Name("fileBroker_files_served_total").Register(registry).Add({});
    cntSize = &prometheus::BuildGauge().Name("fileBroker_queue_size").Register(registry).Add({});
  }
#endif

private:
  std::mutex mutex_;
  unsigned int timeout_;
  struct item {
    item(const std::string& aName = "", unsigned int aLumi = 0) : file(aName), ls(aLumi) {}
    std::string file;
    unsigned int ls;
  };
  std::deque<item> items_;
  std::string state_;
  unsigned int donels_, maxls_;
  std::chrono::steady_clock::time_point last_;
  bool completed_;
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
  prometheus::Counter *cntFound, *cntServed;
  prometheus::Gauge* cntSize;
#endif
};

class DirWatcher {
public:
  DirWatcher(const std::string& path, unsigned int nstreams, SimpleBroker& broker, bool checkEvents)
      : path_(path), nstreams_(nstreams), broker_(&broker), checkEvents_(checkEvents) {
    inotify_fd_ = inotify_init();
    if (inotify_fd_ < 0) {
      perror("inotify_init");
      throw std::runtime_error("Cannot init inotify");
    }

    inotify_wd_ = inotify_add_watch(inotify_fd_, path.c_str(), IN_MOVED_TO);
    printf("Watching %s for new files\n", path.c_str());
  }

  void go() {
    std::thread t(&DirWatcher::poll, this);
    t.detach();
  }

  void poll() {
    while (!broker_->completed()) {
      if (!readMessages())
        break;
    }
  }

protected:
  std::string path_;
  unsigned int nstreams_;
  SimpleBroker* broker_;
  int inotify_fd_, inotify_wd_;
  bool checkEvents_;
  struct StreamQueueItem {
    unsigned int run, ls, index;
    std::string fname0;
    std::vector<unsigned int> streams;
    std::chrono::steady_clock::time_point first, last;
    StreamQueueItem(
        unsigned int arun, unsigned int als, unsigned int aindex, unsigned int stream, const std::string& fname)
        : run(arun), ls(als), index(aindex), streams(1, stream), first(std::chrono::steady_clock::now()), last(first) {
      if (stream == 0)
        fname0 = fname;
    }
  };
  mutable std::list<StreamQueueItem> demuxQueue_;

  bool readMessages() const {
    const unsigned int EVENT_SIZE = sizeof(struct inotify_event);
    const unsigned int BUF_LEN = 1024 * (EVENT_SIZE + 16);
    char buffer[BUF_LEN];
    while (!broker_->completed()) {
      int length = read(inotify_fd_, buffer, sizeof(buffer));

      if (length < 0) {
        perror("read");
        return false;
      }

      for (int i = 0; i < length; i += EVENT_SIZE + reinterpret_cast<inotify_event*>(&buffer[i])->len) {
        struct inotify_event* event = reinterpret_cast<inotify_event*>(&buffer[i]);
        if (event->len) {
          if (event->mask & (IN_CLOSE_WRITE | IN_MOVED_TO)) {
            if (!(event->mask & IN_ISDIR)) {
              std::string fname = event->name;
              if (fname.length() > 4 && fname.substr(fname.length() - 4) == ".raw") {
                std::string in(path_ + "/" + fname);
                if (std::filesystem::exists(in)) {
                  unsigned int run, ls, index, streamid;
                  if (parseFilename(fname, run, ls, index, streamid)) {
                    if (nstreams_ == 0) {
                      if (streamid == 0) {
                        broker_->push(fname, ls);
                      }
                      continue;
                    }
                    if (streamid >= nstreams_)
                      throw std::runtime_error("Bogus stream id " + std::to_string(streamid) + ", when nstreams is " +
                                               std::to_string(nstreams_));
                    if (nstreams_ == 1) {
                      broker_->push(fname, ls);
                    } else {
                      bool found = false;
                      for (auto it = demuxQueue_.begin(), ed = demuxQueue_.end(); it != ed; ++it) {
                        if (it->run == run && it->ls == ls && it->index == index) {
                          found = true;
                          if (std::find(it->streams.begin(), it->streams.end(), streamid) == it->streams.end()) {
                            it->streams.push_back(streamid);
                            if (streamid == 0)
                              it->fname0 = fname;
                            it->last = std::chrono::steady_clock::now();
                            if (it->streams.size() == nstreams_) {
                              if ((!checkEvents_) || checkSameEvents(it->fname0)) {
                                broker_->push(it->fname0, ls);
                              }
                              demuxQueue_.erase(it);
                              break;
                            }
                          }
                        }
                      }
                      if (!found) {
                        demuxQueue_.emplace_back(run, ls, index, streamid, fname);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      auto now = std::chrono::steady_clock::now();
      for (auto it = demuxQueue_.begin(), ed = demuxQueue_.end(); it != ed; ++it) {
        float age = std::chrono::duration<double>(it->last - now).count();
        if (age > 5.0) {
          std::cout << "WARNING: queue item lumi " << it->ls << ", index " << it->index << ", fname0 '" << it->fname0
                    << "' with " << it->streams.size() << "/" << nstreams_ << " streams is stale by " << age
                    << " seconds" << std::endl;
        }
      }

      if (length > 0)
        return true;
    }

    return false;  // unreachable
  }

  bool parseFilename(
      std::string& fname, unsigned int& run, unsigned int& ls, unsigned int& index, unsigned int& streamid) const {
    // in the form run000037_ls0001_index000513[_stream00].raw
    if (fname.length() < 5)
      return false;
    fname = fname.substr(0, fname.length() - 4);  // remove the ".raw";
    auto pos = fname.rfind('_');
    if (pos == std::string::npos || (fname.substr(pos, 7) != "_stream")) {
      return false;
    }
    streamid = std::atoi(fname.substr(pos + 7).c_str());
    std::string work = fname.substr(0, pos);
    pos = work.rfind('_');
    if (pos == std::string::npos || work.substr(pos, 6) != "_index") {
      return false;
    }
    index = std::atoi(work.substr(pos + 6).c_str());
    auto pos2 = work.rfind('_', pos - 1);
    if (pos2 == std::string::npos || work.substr(pos2, 3) != "_ls") {
      return false;
    }
    ls = std::atoi(work.substr(pos2 + 3, pos).c_str());
    if (work.substr(0, 3) != "run") {
      return false;
    }
    run = std::atoi(work.substr(3, pos2 - 3).c_str());
    return true;
  }

  bool checkSameEvents(const std::string& fname0) const {
    auto match = fname0.find("_stream00");
    assert(match != std::string::npos);
    auto fnameBase = fname0.substr(0, match);
    char buff[16];
    unsigned int norbits;
    for (unsigned int s = 0; s < nstreams_; ++s) {
      snprintf(buff, sizeof(buff), "_stream%02u.raw", s);
      std::filesystem::path p = path_ + "/" + fnameBase + buff;
      if (!std::filesystem::exists(p))
        return false;
      std::fstream fin(p.generic_string(), std::ios_base::in | std::ios_base::binary);
      if (!fin.good())
        return false;
      char fileHeader[32];
      fin.read(fileHeader, sizeof(fileHeader));
      if (!fin.good())
        return false;
      if (std::string(fileHeader, fileHeader + 8) != "RAW_0002")
        return false;
      uint32_t norbitsThisFile = *reinterpret_cast<const uint32_t*>(fileHeader + 12);
      if (s == 0) {
        norbits = norbitsThisFile;
      } else {
        if (norbitsThisFile != norbits) {
          std::cout << "Mismatch in number of events between " << fname0 << " (" << norbits << " orbits) and stream "
                    << s << " (" << norbitsThisFile << " orbits)" << std::endl;
          return false;
        }
      }
    }
    return true;
  }
};

int setup_tcp(const char* addr, unsigned int port) {
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("ERROR opening socket");
    return -1;
  }
  int on = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on))) {
    perror("ERROR on setsockopt() SO_REUSEADDR for socket");
    return -1;
  }
  if (ioctl(sockfd, FIONBIO, (char*)&on)) {
    perror("ioctl() for FIONBIO failed");
    close(sockfd);
    return -1;
  }
  struct sockaddr_in serv_addr;
  bzero((char*)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  serv_addr.sin_addr.s_addr = inet_addr(addr);
  if (bind(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
    perror("ERROR on binding");
    return -2;
  }
  printf("Set up server on %s port %u\n", addr, port);
  fflush(stdout);
  listen(sockfd, 5);
  return sockfd;
}

int receive_tcp(int sockfd, const char* addr, unsigned int port, bool waitmsg = false) {
  struct sockaddr_in cli_addr;
  bzero((char*)&cli_addr, sizeof(cli_addr));
  socklen_t clilen = sizeof(cli_addr);
  if (waitmsg) {
    printf("Wait for client on %s port %u\n", addr, port);
    fflush(stdout);
  }
  int newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &clilen);
  if (newsockfd < 0) {
    if (errno != EWOULDBLOCK) {
      perror("ERROR on accept");
    }
    return newsockfd;
  }
  printf("Connection accepted from %s:%d on port %u\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), port);
  fflush(stdout);
  return newsockfd;
}

bool readFromSock(int sockfd, char*& ptr, unsigned int size) {
  unsigned int room = size;
  do {
    int nb = recv(sockfd, ptr, room, 0);
    if (nb < 0) {
      if (errno == EAGAIN)
        continue;
      printf("Error reading from file\n");
      fflush(stdout);
      return false;
    } else if (nb == 0) {
      printf("Connection closed by remote peer\n");
      fflush(stdout);
      return false;
    }
    ptr += nb;
    room -= nb;
    if (nb >= 3 && strncmp(ptr - 3, "\n\r\n", 3) == 0) {
      return true;
    }
  } while (room > 0);
  printf("Buffer full?\n");
  fflush(stdout);
  return false;
}

bool serveConnection(int sockfd, SimpleBroker& broker, bool& keepAlive, bool debug) {
  static const std::regex queryExpr("GET\\s+/popfile\\?runnumber=(\\d+)&pid=(\\d+)(&stopls=(\\d+))?\\s+HTTP/1.1");
  static const std::regex keepAliveExpr("\r\nConnection:\\s+[Kk]eep-[Aa]live\r\n");
  char buff[2048];
  char* ptr = buff;
  if (readFromSock(sockfd, ptr, sizeof(buff))) {
    std::string request(buff, ptr - buff);
    if (debug)
      std::cout << "REQUEST: \n------\n" << request << "-------\n" << std::endl;
    auto firstCRLF = request.find("\r\n");
    auto query = request.substr(0, firstCRLF);
    std::smatch matches;
    if (std::regex_match(query, matches, queryExpr)) {
      unsigned int run = std::stoul(matches.str(1));
      unsigned int pid = std::stoul(matches.str(2));
      unsigned int stop = matches[3].length() ? std::stol(matches.str(4)) : 0;
      if (debug)
        std::cout << "Received request for run " << run << ", pid " << pid << ", maxLS " << stop << std::endl;
      keepAlive = std::regex_search(request, matches, keepAliveExpr);
      std::string file, state;
      unsigned int lumisection = 0, lastls = 0;
      std::stringstream ret;
      ret << "version=2.0.0\n";
      ret << "runnumber=" << run << "\n";
      broker.pop(state, file, lumisection, lastls);
      ret << "state=" << state << "\n";
      if (!file.empty())
        ret << "file=" << file << "\n";
      ret << "lumisection=" << lumisection << "\n";
      ret << "lasteols=" << lastls << "\n";
      std::string reply = ret.str();
      int len = snprintf(
          buff,
          sizeof(buff),
          "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\nContent-Type: text/plain; charset=utf-8\r\n\r\n%s\r\n\r\n",
          reply.length() + 1,
          reply.c_str());
      send(sockfd, buff, len - 1, 0);
      if (debug)
        std::cout << "Reply\n------\n" << buff << "----" << std::endl;
      return true;
    } else {
      printf("Bad request '%s'\n", query.c_str());
      fflush(stdout);
      return false;
    }
  } else {
    return false;
  }
}

void usage(const char* argv0) {
  printf("Usage: %s [options] path\n", argv0);
  printf(" -p port: listener port (default 9876)\n");
  printf(" -a addr: listener addr (default 0.0.0.0)\n");
  printf(" --stream0 : only check for stream 0\n");
  printf(" --demux N : demux N streams (check all streams are available)\n");
  printf(" --checkEvents : when using --demux N, also check that all files have the same number of events.\n");
  printf(" --timeout T : timeout before declaring a LS done (in seconds, default = 5)\n");
  printf(" --prometheus addr : Export stats as prometheus endpoint on addr (e.g. 127.0.0.1:8081)\n");
  printf("\n");
}

int main(int argc, char** argv) {
  if (argc < 2) {
    usage(argv[0]);
    return 1;
  }

  int port = 9876, threads = -1, demux = 1;
  unsigned int timeout = 5;
  std::string addr = "0.0.0.0";
  std::string prometheusExport;
  bool debug = false, stream0 = false, checkEvents = false;

  while (1) {
    static struct option long_options[] = {{"help", no_argument, nullptr, 'h'},
                                           {"demux", required_argument, nullptr, 'd'},
                                           {"addr", required_argument, nullptr, 'a'},
                                           {"port", required_argument, nullptr, 'p'},
                                           {"threads", required_argument, nullptr, 'j'},
                                           {"timeout", required_argument, nullptr, 'T'},
                                           {"prometheus", required_argument, nullptr, 1},
                                           {"stream0", no_argument, nullptr, '0'},
                                           {"checkEvents", no_argument, nullptr, 'C'},
                                           {"verbose", no_argument, nullptr, 'v'},
                                           {nullptr, 0, nullptr, 0}};
    int option_index = 0;
    int optc = getopt_long(argc, argv, "hd:a:j:T:p:v0C", long_options, &option_index);
    if (optc == -1)
      break;

    switch (optc) {
      case 'h':
        usage(argv[0]);
        return 0;
      case 'a':
        addr = std::string(optarg);
        break;
      case 'v':
        debug = true;
        break;
      case '0':
        stream0 = true;
        break;
      case 'C':
        checkEvents = true;
        break;
      case 'd':
        demux = std::atol(optarg);
        break;
      case 'T':
        timeout = std::atol(optarg);
        break;
      case 'p':
        port = std::atol(optarg);
        break;
      case 'j':
        threads = std::atol(optarg);
        break;
      case 1:
        prometheusExport = std::string(optarg);
        break;
      default:
        usage(argv[0]);
        return 1;
    }
  }

  int iarg = optind, narg = argc - optind;
  if (narg != 1) {
    usage(argv[0]);
    return 1;
  }

  std::string path = argv[iarg];
  printf("Configured for %s:%d, servinf from path %s\n", addr.c_str(), port, path.c_str());
  if (threads > 0) {
    printf("Warning: multithreading requested but not implemented yet\n");
  }

  SimpleBroker broker(timeout);
  DirWatcher watcher(path, stream0 ? 0 : demux, broker, checkEvents);

#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
  auto prometheusRegistry = std::make_shared<prometheus::Registry>();
  broker.setUpPrometheus(*prometheusRegistry);
  auto& cntNClients = prometheus::BuildGauge().Name("fileBroker_nclients").Register(*prometheusRegistry).Add({});
  std::unique_ptr<prometheus::Exposer> prometheusExposer;
  if (!prometheusExport.empty()) {
    CPUCounter::start("fileBroker", *prometheusRegistry);
    DirGauge::start("fileBroker", path, false, *prometheusRegistry);
    prometheusExposer = std::make_unique<prometheus::Exposer>(prometheusExport);
    prometheusExposer->RegisterCollectable(prometheusRegistry);
    printf("Exporting prometheus counters on http://%s/metrics\n", prometheusExport.c_str());
  }
#endif

  watcher.go();

  int bindfd = setup_tcp(addr.c_str(), port);  // done only once per job
  if (bindfd < 0)
    return 2;
  unsigned int maxconn = 100;
  std::vector<pollfd> fds(1);
  fds.reserve(maxconn + 1);
  memset(&fds.front(), 0, sizeof(pollfd));
  fds[0].fd = bindfd;
  fds[0].events = POLLIN;
  int pollTimeout = 1 * 60 * 1000;
  int pollret;
  bool hasRun = false;
  while ((pollret = poll(&fds.front(), fds.size(), pollTimeout)) > 0) {
    for (auto& it : fds) {
      if (it.revents == 0)
        continue;
      if (it.revents != POLLIN) {
        printf("ERROR %d on fd %d (%d)\n", int(it.revents), it.fd, int(&it - &fds.front()));
        if (it.fd == bindfd) {
          printf("Shutting down\n");
          close(bindfd);
          return 2;
        } else {
          it.fd = -1;
          continue;
        }
      }
      if (it.fd == bindfd) {
        // serve new connection requests
        while (fds.size() < maxconn) {
          int sockfd = receive_tcp(bindfd, addr.c_str(), port);
          if (sockfd < 0) {
            if (errno != EWOULDBLOCK) {
              perror("Error when accepting socket\n");
              close(bindfd);
              return 3;
            }
            break;
          }
          int one = 1;
          setsockopt(sockfd, SOL_TCP, TCP_NODELAY, (const void*)&one, sizeof(one));
          fds.push_back(pollfd{sockfd, POLLIN, 0});
          hasRun = true;
        }
      } else {
        // serve individual connections
        bool keepAlive = false;
        bool ok = serveConnection(it.fd, broker, keepAlive, debug);
        if (!(ok && keepAlive)) {
          close(it.fd);
          it.fd = -1;
        }
      }
    }  // fds loop
    // cleanup closed connections
    auto firstbad = std::remove_if(fds.begin(), fds.end(), [](const pollfd& p) { return p.fd == -1; });
    if (firstbad != fds.end()) {
      fds.erase(firstbad, fds.end());
    }
#if defined(USE_PROMETHEUS) && USE_PROMETHEUS == 1
    cntNClients.Set(fds.size() - 1);
#endif
    // end if all clients disconnected
    if (hasRun && fds.size() == 1) {
      printf("Exiting after all clients have disconnected\n");
      break;
    }
  }  // poll loop
  if (pollret < 0) {
    perror("Error in polling");
    return 2;
  } else {
    printf("Exiting after timeout\n");
  }
  close(bindfd);
  return 0;
}