#include "UnpackerBase.h"
#include "unpack.h"
#include "UnpackMaker.h"
#include <getopt.h>

void usage() {
  printf("Usage: p2scout-unpacker [ options ] <obj> <kind> <format> infile.dump [infile2.dump ...] [ outfile.root ]\n");
  printf("  obj  := puppi | tkem | tkmu | composite\n");
  printf("  kind := ttree | rntuple | ipcstream | ipcfile \n");
  printf("  puppi ttree   formats  := float | float24 | int | raw64\n");
  printf("  puppi rntuple formats  := floats | floats16 | coll_float | coll_float16 | ints | coll_int | raw64\n");
  printf("  tkem  ttree   formats  := float | float24 | raw96\n");
  printf("  tkem  rntuple formats  := floats | floats16 | coll_float | floats16 | coll_float16 | raw96\n");
  printf("  tkmu  ttree   formats  := float | float24 | int | raw96\n");
  printf("  tkmu  rntuple formats  := floats | floats16 | coll_float | floats16 | coll_float16 | raw96\n");
  printf("  puppi ipc*  formats    := float | float16 | int | raw64\n");
  printf("  tkmu  ipc*  formats    := float | float16\n");
  printf("Options: \n");
  printf("  -j, --threads   N: multithread with N threads\n");
  printf("  -J, --iothreads N: set the capacity of the global I/O thread pool (apache unpackers only)\n");
  printf("  -z algo[,level ] : enable compression\n");
  printf("                     algorithms supported are none, lzma, zlib, lz4, zstd;\n");
  printf("                     default level is 4\n");
  printf("  --batchsize N    : batch size (for apache unpackers only)\n");
  printf("  --cmssw          : assume CMSSW file headers\n");
}

int main(int argc, char **argv) {
  if (argc < 4) {
    usage();
    return 1;
  }

  std::string compressionMethod = "none";
  int compressionLevel = 0, threads = -1;
  bool cmsswHeaders = false;
  UnpackerBase::Spec::Options options;
  while (1) {
    static struct option long_options[] = {{"help", no_argument, nullptr, 'h'},
                                           {"batchsize", required_argument, nullptr, 'b'},
                                           {"threads", required_argument, nullptr, 'j'},
                                           {"iothreads", required_argument, nullptr, 'J'},
                                           {"compression", required_argument, nullptr, 'z'},
                                           {"cmssw", no_argument, nullptr, 1},
                                           {nullptr, 0, nullptr, 0}};
    int option_index = 0;
    int optc = getopt_long(argc, argv, "hj:J:b:z:", long_options, &option_index);
    if (optc == -1)
      break;

    switch (optc) {
      case 'h':
        usage();
        return 0;
      case 1:
        cmsswHeaders = true;
        break;
      case 'b':
        options["batchsize"] = optarg;
        break;
      case 'j':
        threads = std::atoi(optarg);
        break;
      case 'J':
        options["iothreads"] = optarg;
        break;
      case 'z': {
        compressionMethod = std::string(optarg);
        auto pos = compressionMethod.find(",");
        if (pos != std::string::npos) {
          compressionLevel = std::atoi(compressionMethod.substr(pos + 1).c_str());
          compressionMethod = compressionMethod.substr(0, pos);
        } else {
          compressionLevel = 5;
        }
      } break;
      default:
        usage();
        return 1;
    }
  }

  int iarg = optind, narg = argc - optind;
  if (narg < 4) {
    usage();
    return 1;
  }
  printf("Will run %s %s with format %s\n", argv[iarg], argv[iarg + 1], argv[iarg + 2]);
  UnpackerBase::Spec spec(argv[iarg], argv[iarg + 1], argv[iarg + 2], options);
  spec.setCMSSWHeaders(cmsswHeaders);
  spec.setCompression(compressionMethod, compressionLevel);
  if (threads != -1)
    spec.setThreads(threads);

  std::vector<std::string> ins;
  std::string output;
  for (int i = iarg + 3; i < iarg + narg; ++i) {
    std::string fname = argv[i];
    if (UnpackMaker::is_output_fname(fname)) {
      if (!output.empty()) {
        printf("Multiple output files specified in the command line\n");
        return 2;
      }
      output = fname;
    } else {  // assume it's an input file
      ins.emplace_back(argv[i]);
    }
  }

  int ret = 0;
  try {
    std::unique_ptr<UnpackerBase> unpacker = UnpackMaker::make(spec);
    if (!unpacker) {
      printf("Unsupported object type %s, file type %s and/or output format %s\n",
             argv[iarg],
             argv[iarg + 1],
             argv[iarg + 2]);
      return 1;
    }

    auto report = unpacker->unpackFiles(ins, output);
    printReport(report);
  } catch (const std::exception &e) {
    printf("Terminating an exception was raised:\n%s\n", e.what());
    ret = 1;
  }
  return ret;
}
